<?php

use App\Http\Controllers\ProfileController;

use App\Mail\EligibilityMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\VerfiedController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'prevent-back-history'], function () {

    Route::domain("localhost")->group(function () {

        Route::prefix("mail")->group(function () {
            Route::get("confirm", [MailController::class, "confirmMeet"]);
            Route::get("change", [MailController::class, "changeMeet"]);
        });

        Route::get("", function () {
            return view("web.index");
        });


        Route::get('/home',[\App\Http\Controllers\HomeController::class,'home'])->name('home');



        Route::get('actuality', [\App\Http\Controllers\ArticleController::class, 'getarticles'])->name('actuality');

        Route::get("agenda", function () {
            return view("web.agenda");
        })->name('agenda');

        Route::get("calendar", function () {
            return view("web.calendar");
        })->name('calendar');

        Route::get("contact", function () {
            return view("web.contact");
        })->name('contact');

        Route::get("about", function () {
           return view('web.about_us');
        })->name('about');
//    Login Route
        Route::get("login", 'App\Http\Controllers\LoginController@login_page')->name('login');
        Route::post("connexion_trait", 'App\Http\Controllers\LoginController@trait_login')->name('connexion_trait');
//    Login Route
//Register Route
        Route::get("registre", 'App\Http\Controllers\RegisterController@reg_page')->name('registre');
        Route::post("registre_trait", 'App\Http\Controllers\RegisterController@trait_reg')->name('registre_trait');
//Register Route

//    Subvention Route
        Route::get('subevention', function () {
            return view('web.subvention');
        })->name('subevention');

//    Subvention Route


        //Google login
        Route::get('login/google', [\App\Http\Controllers\LoginController::class, 'redirectToGoogle'])->name('login.google');
        Route::get('login/google/callback', [\App\Http\Controllers\LoginController::class, 'handleGoogleCallback']);
        //Google login

        //facebook login
        Route::get('login/facebook', [\App\Http\Controllers\LoginController::class, 'redirectToFacebook'])->name('login.facebook');
        Route::get('login/facebook/callback', [\App\Http\Controllers\LoginController::class, 'handleFacebookCallback']);
        //facebook login

        //linkedin login
        Route::get('login/linkedin', [\App\Http\Controllers\LoginController::class, 'redirectToLinkedin'])->name('login.linkedin');
        Route::get('login/linkedin/callback', [\App\Http\Controllers\LoginController::class, 'handleLinkedinCallback']);
        //linkedin login

        //Profile Page
        Route::get('profile', [ProfileController::class, 'profile_page'])->name('profile');
        //Profile Page
        Route::post('contact',[\App\Http\Controllers\ContactController::class,'contact'])->name('contact');

        //EditProfile
        Route::get('/edit_profile',[ProfileController::class,'edit_profile_page']);
        Route::post('updt_profile',[ProfileController::class,'updt_profile'])->name('updt_profile');
        //EditProfile

        Route::post('not_send',[\App\Http\Controllers\HomeController::class,'not_send'])->name('not_send');


        Route::get('testemail',[VerfiedController::class,"sendSignupEmail"]);

        //déconnexion
        Route::get('deconnexion', [ProfileController::class, 'deconnexion'])->name('déconnexion');
        //déconnexion

        // About us
        Route::get('a_propos', function () {
            return view('web.about_us');
        })->name('a_propos');
        // About us

        Route::get('map', function () {
            return view('web.map');
        });

        //popup
        Route::get('pop',function ()
        {
            return view('web.popup');
        });
        //popup

        //forgot password
            //  Route::post('mot_passe_oubliee',function (){
            //     return view('web.forgot_p');
            //  });
            Route::get('mot_passe_oubliee',function (){
                     return view('web.forgot_p');
                  });
        //forgot password


        //search subvention

        Route::get("search", 'App\Http\Controllers\SearchController@search')->name('search');
      //search subvention

    });
});

Route::domain('crm.localhost')->group(function () {
    Route::get('/{any}', function () {
        return view('crm.app');
    })->where('any', '.*');
});

/* Route::get('/{any}', function(){
    return view('app');
})->where('any','.*'); */

Route::get("/test/email", function () {
    //Mail::send(new EligibilityMail());
    //return new EligibilityMail();
});
//page normandie
Route::get('/normandie', function () {
    return view('web.normandie');
});
//page test d'égibilité
Route::get('/test_egibilite', function () {
    return view('web.test_egibilite');
});
//page chéque e commerce
Route::get('/cheque_ecommerce', function () {
    return view('web.cheque_ecommerce');
});
