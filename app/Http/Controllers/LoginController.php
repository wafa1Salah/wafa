<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login_page()
    {
        return view('web.connexion.login');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->user();
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();
    }

    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    public function handleLinkedinCallback()
    {
        $user = Socialite::driver('linkedin')->user();
    }





    public function trait_login(Request $request)
    {
        request()->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        $user = User::where('email', $request->email)->get()->first();
        if ($user->email_verified_at == null) {
            return redirect()->back()->with(['error1' => 'Il faux vérifier votre compte']);
        } else {
            $resultat = auth()->attempt([
                'email' => request('email'),
                'password' => request('password'),
            ]);

            if ($resultat) {
                if (auth()->guest()) {
                    return view("web.home");
                } else {
                    $articles = DB::table("articles")->join("pictures", "articles.id", "articles_id")->limit(3)->get();

                    return view("web.home", ["articles" => $articles]);
                }
            } else {
                return redirect()->back()->with(['error' => 'Verifier vos informations !']);
            }
        }
    }
}
