<?php

namespace App\Http\Controllers;

use App\Models\Addresses;
use App\Models\Social_account;
use App\Mail\SignupEmail;
use App\Models\User;
use App\Models\Picture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Self_;

class RegisterController extends Controller
{
    public function reg_page()
    {
        return view('web.connexion.register');
    }
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    protected function redirectTo()
    {
        $user = auth()->user();
        switch ($user->role) {
            case "tpe":
                $client = new User();
                $client->user_id = auth()->user()->id;

                $client->save();
                return  route("profile");
                break;
            case "ag":
                $client = new User();
                $client->user_id = auth()->user()->id;
                $client->save();
                return route("profile");
                break;
            case "col":
                $client = new User();
                $client->user_id = auth()->user()->id;
                $client->save();
                return route("profile");
                break;
            default:
                return route("profile");
        }
    }
    public function trait_reg(Request $request)
    {
       /*  $rules =    [
            'email' => 'required | unique:users,email',
            'password' => 'confirmed|min:8',
        ];

        $messages =       [
            'email.required' => 'Ce champ est obligatoire',
            'email.unique' => 'Ce champ est unique',
            'password.confirmed' => 'La confirmation du mot de passe doit correspondre au mot de passe',
            'password.min' => 'Pour des raisons de sécurité, votre mot de passe doit faire :min caractères.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInputs($request->all());
        }
 */

        if ($request->accept == null)
            return redirect()->back()->with(['error1' => "Il faut accepter les CGU et la Politque de site"]);
        else {
            $user = new User();
            $user->first_name = $request->first_name;
            //        $user->last_name= $request->last_name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role = $request->inlineRadioOptions;

            $user->save();
            $adresse = new Addresses();
            $adresse->users_id = $user->id;
            $adresse->save();
            $picture = new Picture();
            $picture->users_id = $user->id;
            $picture->link = "https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg";
            $picture->save();

            $request->session()->put('firstname', $user->first_name);
            $request->session()->put('email', $user->email);
            Mail::send(new SignupEmail());
            //        $social_accounts=new Social_account();
            //        $social_accounts->user_id=$user->id;
            //        $social_accounts->save();

            if ($request->inlineRadioOptions == 'tpe' || $request->inlineRadioOptions == 'age' || $request->inlineRadioOptions == 'col') {
                return redirect('/login')->with(['success' => "Nous avons vous envoyer un e-mail pour verifier votre compte !"]);
            } else {
                return redirect()->back()->with(['error2' => "Il faut choisir votre type d'entreprise!"]);
            }
        }
    }
}
