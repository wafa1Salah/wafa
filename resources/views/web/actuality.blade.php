@extends('web.layouts.app')

@section('content')


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>



<div class="actuality_container">
    <div class="actuality_wrapper">
        <div class="container pt-4 px-4 g-0">
            <div class="row justify-content-center">
                <div class="actuality_content col-md-12" style=" display:contents;flex-direction: row; justify-content: center;">

                    <div class="row mb-3 justify-content-center">
                        <div class="col-auto">
                            <h1 style="font-weight: bold; color: rgb(33, 7, 94)" class="m-0">
                                Actualité
                            </h1>
                        </div>
                    </div>

                    <div class="row mb-5 g-4 justify-content-center">
                        @foreach($articles as $article => $val)

                        @if($article % 2 == 0)
                        <div class="col-md-4 col-lg-4 col-xl-4">
                            <div style="overflow: hidden" class="act-wrapper shadow rounded-3 bg-white">
                                <div style="height: 230px;" class="act-img">
                                    <img style="width: 100%; height:100%; object-fit:cover" src="{{$val->link}}" alt="" />
                                </div>
                                <div style="height: 190px;" class="act-content p-2">
                                    <div class="act-title">
                                        <h5> {{$val->title}}| </h5>
                                    </div>
                                      <?php    $rest = substr("$val->desc",0,50);
                                      $rest1 = substr("$val->desc",50,strlen($val->desc));
                                        ?>
                                    <div class="act-desc">
                                        <p class="m-0"> {{$rest}}...</p>
                                    </div>
                                    <div class="act-button d-flex flex-row justify-content-end">
                                        <button style="border: none; background: transparent" >
                                          <p class="readMore">  Lire la suite..</p>
                                        </button>
                                    </div>
                                    <span style="border-left: 2px solid black; padding-left: 10px">{{$val->created_at}}</span>
                                </div>
                            </div>
                        </div>

                        @else

                        <div class="col-md-4 col-lg-4 col-xl-4">
                            <div style="overflow: hidden" class="act-wrapper shadow rounded-3 bg-white">
                                <div style="height: 190px;" class="act-content p-2">
                                    <div class="act-title">
                                        <h5>
                                            {{$val->title}}|
                                        </h5>
                                    </div>                                    
                                    <?php    $rest = substr("$val->desc",0,50);  ?>
                                    <div class="act-desc">
                                        <p class="m-0">
                                            {{$rest}}...
                                        </p>
                                    </div>

                                    <div class="act-button d-flex flex-row justify-content-end">
                                        <button style="border: none; background: transparent">
                                            Lire la suite..
                                        </button>
                                    </div>
                                    <span style="border-left: 2px solid black; padding-left: 10px">{{$val->created_at}}</span>
                                </div>
                                <div style="height: 230px;" class="act-img">
                                    <img style="width: 100%; height:100%; object-fit:cover" src="{{$val->link}}" alt="" />
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>



                    <div class="row justify-content-start mb-5">
                        <div class="col-md-12">
                            <div class="row justify-content-start">
                                <div class="col">
                                    <h4  class="pull-left" style="font-weight: 700; margin-left:0; " class="m-0">Autre actualité</h4>
                                </div>
                                <div class="col-auto">
                                    <div class="button-wrapper">
                                        <button value="Click Me" class="btn btn-left">
                                            < </button>
                                                <button value="Click Me" class="btn btn-right"></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <style>
                        h2 {
                            text-align: center;
                            padding: 20px;
                        }

                        /* Slider */

                        .slick-slide {
                            margin: 0px 20px;
                        }

                        .slick-slide img {
                            width: 100%;
                        }

                        .slick-slider {
                            position: relative;
                            display: block;
                            box-sizing: border-box;
                            -webkit-user-select: none;
                            -moz-user-select: none;
                            -ms-user-select: none;
                            user-select: none;
                            -webkit-touch-callout: none;
                            -khtml-user-select: none;
                            -ms-touch-action: pan-y;
                            touch-action: pan-y;
                            -webkit-tap-highlight-color: transparent;
                        }

                        .slick-list {
                            position: relative;
                            display: block;
                            overflow: hidden;
                            margin: 0;
                            padding: 0;
                        }

                        .slick-list:focus {
                            outline: none;
                        }

                        .slick-list.dragging {
                            cursor: pointer;
                            cursor: hand;
                        }

                        .slick-slider .slick-track,
                        .slick-slider .slick-list {
                            -webkit-transform: translate3d(0, 0, 0);
                            -moz-transform: translate3d(0, 0, 0);
                            -ms-transform: translate3d(0, 0, 0);
                            -o-transform: translate3d(0, 0, 0);
                            transform: translate3d(0, 0, 0);
                        }

                        .slick-track {
                            position: relative;
                            top: 0;
                            left: 0;
                            display: block;
                        }

                        .slick-track:before,
                        .slick-track:after {
                            display: table;
                            content: '';
                        }

                        .slick-track:after {
                            clear: both;
                        }

                        .slick-loading .slick-track {
                            visibility: hidden;
                        }

                        .slick-slide {
                            display: none;
                            float: left;
                            height: 100%;
                            min-height: 1px;
                        }

                        [dir='rtl'] .slick-slide {
                            float: right;
                        }

                        .slick-slide img {
                            display: block;
                        }

                        .slick-slide.slick-loading img {
                            display: none;
                        }

                        .slick-slide.dragging img {
                            pointer-events: none;
                        }

                        .slick-initialized .slick-slide {
                            display: block;
                        }

                        .slick-loading .slick-slide {
                            visibility: hidden;
                        }

                        .slick-vertical .slick-slide {
                            display: block;
                            height: auto;
                            border: 1px solid transparent;
                        }

                        .slick-arrow.slick-hidden {
                            display: none;
                        }
                    </style>

                    <script>
                        $(document).ready(function() {
                            $('.customer-logos').slick({
                                slidesToShow: 6,
                                slidesToScroll: 1,
                                autoplay: true,
                                autoplaySpeed: 1500,
                                arrows: false,
                                dots: false,
                                pauseOnHover: false,
                                responsive: [{
                                    breakpoint: 768,
                                    settings: {
                                        slidesToShow: 4
                                    }
                                }, {
                                    breakpoint: 520,
                                    settings: {
                                        slidesToShow: 3
                                    }
                                }]
                            });
                        });
                    </script>





                    <div class="container">
                        <section class="customer-logos slider">
                            @foreach($article2 as $a)
                            <div class="slide"><img src="{{$a->link}}"> </div>
                            <div class="row m-0 g-0">
                                <div style="width: 400px; height:35px; overflow:hidden" class="title">
                                    <h6 style="font-size: 14px; height:100%; width:100%" class="m-0">{{$a->title}}</h6>
                                </div>
                                <div style="width: 100%; height:55px; overflow:hidden" class="desc">
                                    <p style="font-size: 12px; width: 100%; height:100%" class="m-0">{{$a->desc}}</p>
                                </div>
                            </div>
                            @endforeach
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection