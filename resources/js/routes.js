import VueRouter from 'vue-router';
import Home from './pages/Home';
import Eligibility from './pages/Eligibility';
import Clients from './pages/Clients';
import Profile from './pages/Profile';
let routes = new VueRouter({
    mode:"history",
    routes:[
        /* {
            path:"", 
            name:"accueil", 
            meta: { title: 'CPN - Accueil',reload: true, }, 
            component:Home,
            beforeEnter:(to,from,next)=>{
                window.document.title = to.meta && to.meta.title ? to.meta.title : 'CPN';
                (localStorage.getItem("token"))?next({path:"/eligibility"}):next();
            }
        }, */
        {
            path:'/eligibility', 
            name:"eligibility", 
            meta: { title: "CPN - Test d'éligibilité", reload: true, },  
            component:Eligibility,
            beforeEnter(to,from,next){
                window.document.title = to.meta && to.meta.title ? to.meta.title : 'CPN';
                (localStorage.getItem("token"))?next():next({path:""});
            },
        },
        /* {
            path:'/clients', 
            name:"clients", 
            meta: { title: "CPN - Liste client" },  
            component:Clients,
            beforeEnter(to,from,next){
                window.document.title = to.meta && to.meta.title ? to.meta.title : 'CPN';
                (localStorage.getItem("token"))?next():next({path:""});
            },
            
        }, */
        {
            path:'', 
            name:"profile", 
            meta: { title: "CPN - Profile" },  
            component:Profile,
            beforeEnter:(to,from,next)=>{
                window.document.title = to.meta && to.meta.title ? to.meta.title : 'CPN';
                (localStorage.getItem("token"))?next():next({path:""});
            }
        },
        /* {
            path:'/logout', 
            name:"logout", 
            meta: { title: "CPN - logout", },  
            component:Clients,
            beforeEnter(to,from,next){
                localStorage.clear();
                next({path:"/"})
                
            }
        }, */
    ]
})

export default routes;