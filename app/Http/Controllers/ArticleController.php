<?php

namespace App\Http\Controllers;
use App\Models\Article;
use App\Models\Picture;
use Illuminate\Http\Request;
use App\Http\Resources\WEB\ArticlesResources;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
  public function getarticles(){
   
    $articles=DB::table("articles")->join("pictures","articles.id","articles_id")->orderby("created_at","desc")->limit(3)->get();

    $article2=DB::table("articles")->join("pictures","articles.id","articles_id")->limit(10)->get();
    $articles = ArticlesResources::collection($articles);

    return view("web.actuality",["articles"=>$articles,"article2"=>$article2]);
  }
}
