<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AgenceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->role != "ag"){

            abort(401);
        }
        return $next($request);
    }
}
