<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Mail\MailComment;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact(Request $request)
    {

        $contact = new Contact();
        $contact->nom = $request->nom;
        $contact->prenom = $request->prenom;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();
        $request->session()->put('message',$request->message);

        Mail::send(new ContactMail());

        return redirect()->back()->with(['success' => 'Merci de Nous Contactez!']);

    }
}
