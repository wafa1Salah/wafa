require('./bootstrap');

import Vue from 'vue';
import App from "./App.vue";
import VueRouter from 'vue-router';
import routes from './routes';
import VueToast from 'vue-toast-notification';
import Popover from 'vue-js-popover';
import VModal from 'vue-js-modal';
import VueTimePicker from 'vue2-timepicker';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faEye, faTrash, faCog, faCalendarAlt, faCalendar, faBook, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(faEye, faCog, faTrash, faCalendarAlt, faCalendar, faBook, faBookOpen);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('vue-timepicker', VueTimePicker);

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(Popover, { tooltip: true })
Vue.use(VModal, { dialog: true })
Vue.use(VueToast);

const app = new Vue({
    el: '#app',
    router : routes,
    render: h=>h(App),
});

