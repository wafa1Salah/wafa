-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 20 sep. 2021 à 12:45
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cpn-aides-aux-entreprises`
--

-- --------------------------------------------------------

--
-- Structure de la table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `activities`
--

INSERT INTO `activities` (`id`, `name`, `description`) VALUES
(1, 'Agriculture', NULL),
(2, 'Agroalimentaire', NULL),
(3, 'Ameublement', NULL),
(4, 'Art', NULL),
(5, 'Assurance', NULL),
(6, 'Automobile', NULL),
(7, 'Banque', NULL),
(8, 'Batiment', NULL),
(9, 'Bois', NULL),
(10, 'BTP', NULL),
(11, 'Carton', NULL),
(12, 'Chimie', NULL),
(13, 'Commerce', NULL),
(14, 'Communication', NULL),
(15, 'Distribution', NULL),
(16, 'Economie Sociale', NULL),
(17, 'Edition', NULL),
(18, 'Electricité', NULL),
(19, 'Electronique', NULL),
(20, 'Etudes et conseils', NULL),
(21, 'Fabrication', NULL),
(22, 'Habillement / Chaussure', NULL),
(23, 'Habitat social', NULL),
(24, 'Hotellerie', NULL),
(25, 'Imprimerie', NULL),
(26, 'Industrie', NULL),
(27, 'Industrie pharmaceutique', NULL),
(28, 'Informatique', NULL),
(29, 'Logistique', NULL),
(30, 'Machines et équipements', NULL),
(31, 'Magasins', NULL),
(32, 'Matériaux de construction', NULL),
(33, 'Métallurgie', NULL),
(34, 'Multimédia', NULL),
(35, 'Mutualité', NULL),
(36, 'Négoce', NULL),
(37, 'Papier', NULL),
(38, 'Parachimie', NULL),
(39, 'Plastique / Caoutchouc', NULL),
(40, 'Presse', NULL),
(41, 'Productions', NULL),
(42, 'Propreté', NULL),
(43, 'Protection sociale', NULL),
(44, 'Restauration', NULL),
(45, 'Santé', NULL),
(46, 'Services aux entreprises', NULL),
(47, 'Télécoms', NULL),
(48, 'Textile', NULL),
(49, 'Transports', NULL),
(50, 'Travail du métal', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `activities_categories`
--

CREATE TABLE `activities_categories` (
  `id` int(11) NOT NULL,
  `activities_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `addresses`
--

INSERT INTO `addresses` (`id`, `users_id`, `contacts_id`, `companies_id`, `address`, `region`, `city`, `zipcode`, `department`, `country`) VALUES
(1, 1, 1, NULL, '31 Rue Peclet', 'Bourgogne-Franche-Comté', 'Paris', '89', 'Yonne', 'France'),
(2, 1, 2, NULL, '31 Rue Peclet', 'Hauts-de-France', 'Paris', '60', 'Oise', 'France'),
(3, 1, 3, NULL, '31 Rue Peclet', 'Ile-de-France', 'Paris', '77', 'Seine-et-Marne', 'France'),
(4, 1, 4, NULL, '31 Rue Peclet', 'Ile-de-France', 'Paris', '91', 'Essonne', 'France'),
(5, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `title`, `desc`, `created_at`, `verified_at`) VALUES
(1, 'Lorem ipsum dolor sit', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', '2021-09-08 00:08:11', NULL),
(2, 'Lorem ipsum dolor ', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', '2021-09-07 22:00:00', NULL),
(3, 'Lorem ipsum dolor sit', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', '2021-09-08 08:26:25', NULL),
(4, 'dhskjzhe', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', '2021-09-08 09:00:49', NULL),
(5, 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', NULL, NULL),
(6, 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', NULL, NULL),
(7, 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', NULL, NULL),
(8, 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', NULL, NULL),
(9, 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', NULL, NULL),
(10, 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus praesentium voluptatibus iusto nulla sapiente, eligendi in possimus. Impedit tempora itaque odit accusantium debitis, provident expedita voluptate molestiae voluptatibus iusto nihil.', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `calendar_events`
--

CREATE TABLE `calendar_events` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `calendar_events`
--

INSERT INTO `calendar_events` (`id`, `users_id`, `contacts_id`, `title`, `description`, `date`, `color`, `confirmed`) VALUES
(1, 1, 1, 'test crm', NULL, '2021-09-08 06:30:00', 'blue', 0),
(2, 1, 2, 'khalil mecha', NULL, '2021-09-10 10:00:00', 'blue', 0),
(3, 1, 3, 'test crm', NULL, '2021-09-09 11:00:00', 'blue', 0),
(4, 1, 4, 'test crm', NULL, '2021-09-15 11:00:00', 'blue', 0);

-- --------------------------------------------------------

--
-- Structure de la table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `activities_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `salaries` varchar(255) DEFAULT NULL,
  `siret` varchar(255) DEFAULT NULL,
  `siren` varchar(255) DEFAULT NULL,
  `naf` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `turnover` varchar(255) DEFAULT NULL,
  `turnovers_id` int(11) DEFAULT NULL,
  `state_help` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `companies`
--

INSERT INTO `companies` (`id`, `contacts_id`, `activities_id`, `name`, `status`, `salaries`, `siret`, `siren`, `naf`, `phone`, `turnover`, `turnovers_id`, `state_help`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 'aaa', 'SASU', 'de 0 à 5 Personnes', '83174545000021', '831745450', '1813Z', '200587838', '30', 10, 'Chéque numérique et aide numérique de votre région', '2021-09-06 09:16:26', '2021-09-06 09:16:26'),
(2, 2, 5, 'jobid', 'SASU', 'de 0 à 5 Personnes', '83174545000021', '831745450', '1813Z', '20058738', '30', 6, 'Chéque numérique et aide numérique de votre région', '2021-09-07 10:04:49', '2021-09-07 10:04:49'),
(3, 3, 4, 'aaaa', 'SASU', 'de 0 à 5 Personnes', '83174545000021', '831745450', '1813Z', '20058738', '30', 7, 'Chéque numérique et aide numérique de votre région', '2021-09-07 10:51:49', '2021-09-07 10:51:49'),
(4, 4, 5, 'jobid', 'SASU', 'de 0 à 5 Personnes', '83174545000021', '831745450', '1813Z', '20058738', '20', 8, 'Chéque numérique et aide numérique de votre région', '2021-09-07 13:59:38', '2021-09-07 13:59:38');

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `comment` longtext DEFAULT NULL,
  `situation` varchar(255) DEFAULT NULL,
  `lead` tinyint(4) NOT NULL DEFAULT 0,
  `confirmed` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`id`, `users_id`, `first_name`, `last_name`, `email`, `phone`, `position`, `comment`, `situation`, `lead`, `confirmed`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 'crm', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, NULL, 1, 0, '2021-09-06 09:16:26', '2021-09-06 09:16:26'),
(2, 1, 'khalil', 'mecha', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, NULL, 1, 0, '2021-09-07 10:04:48', '2021-09-07 10:04:48'),
(3, 1, 'test', 'crm', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, NULL, 1, 0, '2021-09-07 10:51:49', '2021-09-07 10:51:49'),
(4, 1, 'test', 'crm', 'klilmecha@gmail.com', '20058738', 'Gérant', NULL, NULL, 1, 0, '2021-09-07 13:59:38', '2021-09-07 13:59:38');

-- --------------------------------------------------------

--
-- Structure de la table `counters`
--

CREATE TABLE `counters` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `elapsed_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `development`
--

CREATE TABLE `development` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `have_website` varchar(255) DEFAULT NULL,
  `website_type` varchar(255) DEFAULT NULL,
  `website_value` varchar(255) DEFAULT NULL,
  `website_link` varchar(255) DEFAULT NULL,
  `website_dev_date` varchar(255) DEFAULT NULL,
  `have_crm` varchar(255) DEFAULT NULL,
  `crm_type` varchar(255) DEFAULT NULL,
  `crm_dev` varchar(255) DEFAULT NULL,
  `crm_name` varchar(255) DEFAULT NULL,
  `erp_name` varchar(255) DEFAULT NULL,
  `crm_dev_date` varchar(255) DEFAULT NULL,
  `agency_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `development`
--

INSERT INTO `development` (`id`, `contacts_id`, `have_website`, `website_type`, `website_value`, `website_link`, `website_dev_date`, `have_crm`, `crm_type`, `crm_dev`, `crm_name`, `erp_name`, `crm_dev_date`, `agency_name`) VALUES
(1, 1, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 'non', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `cpn_id` int(11) DEFAULT NULL,
  `regional_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `experiences`
--

CREATE TABLE `experiences` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `experiences`
--

INSERT INTO `experiences` (`id`, `name`, `description`) VALUES
(1, 'débutant', NULL),
(2, 'semi-expérimenté', NULL),
(3, 'expérimenté', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `investment`
--

CREATE TABLE `investment` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `transitions` longtext DEFAULT NULL,
  `budget` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `investment`
--

INSERT INTO `investment` (`id`, `contacts_id`, `service_id`, `transitions`, `budget`) VALUES
(1, 1, 25, '', '24000'),
(2, 2, 22, '', '3500'),
(3, 3, 25, '', '22800'),
(4, 4, 25, '', '23400');

-- --------------------------------------------------------

--
-- Structure de la table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `descritpion` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `levels`
--

INSERT INTO `levels` (`id`, `name`, `descritpion`) VALUES
(1, 'utilisateur', NULL),
(2, 'editeur', NULL),
(3, 'conseiller', NULL),
(4, 'confirmateur', NULL),
(5, 'superviseur', NULL),
(6, 'admin', NULL),
(9, 'superamdin', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `zoom_id` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL,
  `link` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'azgjSc6klnnpidJrhOzLdKHWmAvVI5uqMHIhWvan', NULL, 'http://localhost', 1, 0, 0, '2021-07-18 19:50:01', '2021-07-18 19:50:01'),
(2, NULL, 'Laravel Password Grant Client', 'UsQI7Gso46KuMvadBKBPaxyCqV71kJfVfASHEDeg', 'users', 'http://localhost', 0, 1, 0, '2021-07-18 19:50:01', '2021-07-18 19:50:01');

-- --------------------------------------------------------

--
-- Structure de la table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-07-18 19:50:01', '2021-07-18 19:50:01');

-- --------------------------------------------------------

--
-- Structure de la table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `pages`
--

INSERT INTO `pages` (`id`, `name`, `link`) VALUES
(1, 'home', 'https://www.linkedin.com/pages-extensions/FollowCompany?id=76078573&counter=bottom');

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `articles_id` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `pictures`
--

INSERT INTO `pictures` (`id`, `articles_id`, `users_id`, `name`, `link`) VALUES
(3, 1, 1, 'photo.jpg', 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(4, 2, 3, 'photo.jpg', 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(5, 3, 2, 'photo.jpg', 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(6, 4, 1, 'photo.jpg', 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(7, NULL, 4, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(8, NULL, 5, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg'),
(9, NULL, 5, NULL, 'https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `privileges`
--

CREATE TABLE `privileges` (
  `id` int(11) NOT NULL,
  `levels_id` int(11) NOT NULL,
  `pages_id` int(11) NOT NULL,
  `edit` tinyint(4) NOT NULL DEFAULT 1,
  `update` tinyint(4) NOT NULL DEFAULT 1,
  `delete` tinyint(4) NOT NULL DEFAULT 1,
  `add` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `regions`
--

INSERT INTO `regions` (`id`, `name`) VALUES
(1, 'Nouvelle-Aquitaine'),
(2, 'Ile-de-France'),
(3, 'Hauts-de-France'),
(4, 'Auvergne-Rhone-Alpes'),
(5, 'Normandie'),
(6, 'Grand Est'),
(7, 'Pays de la Loire'),
(8, 'La Réunion'),
(9, 'Martinique'),
(10, 'Guadeloupe');

-- --------------------------------------------------------

--
-- Structure de la table `regions_nafs`
--

CREATE TABLE `regions_nafs` (
  `id` int(11) NOT NULL,
  `regions_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `excluded` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `regions_nafs`
--

INSERT INTO `regions_nafs` (`id`, `regions_id`, `code`, `excluded`) VALUES
(1, 1, '0111Z', 1),
(2, 1, '0112Z', 1),
(3, 1, '0113Z', 1),
(4, 1, '0114Z', 1),
(5, 1, '0115Z', 1),
(6, 1, '0116Z', 1),
(7, 1, '0119Z', 1),
(8, 1, '0121Z', 1),
(9, 1, '0122Z', 1),
(10, 1, '0123Z', 1),
(11, 1, '0124Z', 1),
(12, 1, '0125Z', 1),
(13, 1, '0126Z', 1),
(14, 1, '0127Z', 1),
(15, 1, '0128Z', 1),
(16, 1, '0129Z', 1),
(17, 1, '0130Z', 1),
(18, 1, '0141Z', 1),
(19, 1, '0142Z', 1),
(20, 1, '0143Z', 1),
(21, 1, '0144Z', 1),
(22, 1, '0145Z', 1),
(23, 1, '0146Z', 1),
(24, 1, '0147Z', 1),
(25, 1, '0149Z', 1),
(26, 1, '0150Z', 1),
(27, 1, '0161Z', 1),
(28, 1, '0162Z', 1),
(29, 1, '0163Z', 1),
(30, 1, '0164Z', 1),
(31, 1, '0170Z', 1),
(32, 1, '0311Z', 1),
(33, 1, '0312Z', 1),
(34, 1, '0321Z', 1),
(35, 1, '0322Z', 1),
(36, 1, '9604Z', 1),
(37, 1, '9609Z', 1),
(38, 1, '4110A', 1),
(39, 1, '4110B', 1),
(40, 1, '4110C', 1),
(41, 1, '4110D', 1),
(42, 1, '6810Z', 1),
(43, 1, '6820A', 1),
(44, 1, '6820B', 1),
(45, 1, '6831Z', 1),
(46, 1, '6832A', 1),
(47, 1, '6832B', 1),
(48, 1, '6411Z', 1),
(49, 1, '6419Z', 1),
(50, 1, '6420Z', 1),
(51, 1, '6430Z', 1),
(52, 1, '6491Z', 1),
(53, 1, '6492Z', 1),
(54, 1, '6499Z', 1),
(55, 1, '8610Z', 1),
(56, 1, '8621Z', 1),
(57, 1, '8622A', 1),
(58, 1, '8622B', 1),
(59, 1, '8622C', 1),
(60, 1, '8623Z', 1),
(61, 1, '8690A', 1),
(62, 1, '8690B', 1),
(63, 1, '8690C', 1),
(64, 1, '8690D', 1),
(65, 1, '8690E', 1),
(66, 1, '8690F', 1),
(67, 1, '8510Z', 1),
(68, 1, '8520Z', 1),
(69, 1, '8531Z', 1),
(70, 1, '8510Z', 1),
(71, 1, '8520Z', 1),
(72, 1, '8531Z', 1),
(73, 1, '8532Z', 1),
(74, 1, '8541Z', 1),
(75, 1, '8542Z', 1),
(76, 1, '8551Z', 1),
(77, 1, '8552Z', 1),
(78, 1, '8553Z', 1),
(79, 1, '8559A', 1),
(80, 1, '8559B', 1),
(81, 1, '8560Z', 1),
(82, 1, '6910Z', 1),
(83, 1, '6920Z', 1),
(84, 1, '7010Z', 1),
(85, 1, '7111Z', 1),
(86, 1, '7112A', 1),
(87, 1, '7311Z', 1),
(88, 1, '7312Z', 1),
(89, 1, '7320Z', 1),
(90, 1, '7311Z', 1),
(91, 1, '7312Z', 1),
(92, 1, '7320Z', 1),
(93, 1, '7420Z', 1),
(94, 1, '7500Z', 1),
(95, 2, '4773Z', 1),
(96, 2, '4774Z', 1),
(97, 2, '4791B', 1),
(98, 2, '4932Z', 1),
(99, 2, '6312Z', 1),
(100, 2, '6419Z', 1),
(101, 2, '6430Z', 1),
(102, 2, '6499Z', 1),
(103, 2, '6511Z', 1),
(104, 2, '6512Z', 1),
(105, 2, '6520Z', 1),
(106, 2, '6530Z', 1),
(107, 2, '6611Z ', 1),
(108, 2, '6612Z', 1),
(109, 2, '6619A', 1),
(110, 2, '6619B', 1),
(111, 2, '6621Z', 1),
(112, 2, '6622Z', 1),
(113, 2, '6629Z ', 1),
(114, 2, ' 6630Z', 1),
(115, 2, ' 6831Z', 1),
(116, 2, '6910Z ', 1),
(117, 2, '6920Z', 1),
(118, 2, '7120A', 1),
(119, 2, '7500Z ', 1),
(120, 2, '8610Z', 1),
(121, 2, '8621Z', 1),
(122, 2, '8622A', 1),
(123, 2, '8622B ', 1),
(124, 2, '8622C', 1),
(125, 2, '8623Z', 1),
(126, 2, '8690A', 1),
(127, 2, '8690B', 1),
(128, 2, '8690C ', 1),
(129, 2, '8690D ', 1),
(130, 2, '8690E ', 1),
(131, 2, '8690F', 1),
(132, 2, '8710C', 1),
(133, 2, '8411Z', 1),
(134, 2, '8412Z', 1),
(135, 2, '8421Z', 1),
(136, 2, '8422Z', 1),
(137, 2, '8423Z', 1),
(138, 2, '8424Z', 1),
(139, 2, '8425Z', 1),
(140, 2, '8430A', 1),
(141, 2, '8430B', 1),
(142, 2, '8430C ', 1),
(143, 2, '8510Z', 1),
(144, 2, '8520Z', 1),
(145, 2, '8531Z ', 1),
(146, 2, '8532Z', 1),
(147, 2, '8541Z', 1),
(148, 2, '8542Z', 1),
(149, 2, '8551Z', 1),
(150, 2, '8552Z ', 1),
(151, 2, '8553Z', 1),
(152, 2, '8559A', 1),
(153, 2, '8559B', 1),
(154, 2, '8560Z', 1),
(155, 7, '8411Z', 1),
(156, 7, '8412Z', 1),
(157, 7, '8413Z', 1),
(158, 7, '8421Z', 1),
(159, 7, '8422Z', 1),
(160, 7, '8423Z', 1),
(161, 7, '8424Z', 1),
(162, 7, '8425Z', 1),
(163, 7, '8430A', 1),
(164, 7, '8430B', 1),
(165, 7, '8430C', 1),
(166, 7, '4791B', 1),
(167, 7, '4773Z', 1),
(168, 7, '4774Z', 1),
(169, 7, '4778A', 1),
(170, 7, '4791A', 1),
(171, 7, '4791B', 1),
(172, 7, '4932Z', 1),
(173, 7, '6312Z', 1),
(174, 7, '6411Z', 1),
(175, 7, '6492Z', 1),
(176, 7, '6499Z', 1),
(177, 7, '6419Z', 1),
(178, 7, '6430Z', 1),
(179, 7, '6491Z', 1),
(180, 7, '6630Z', 1),
(181, 7, '6831Z', 1),
(182, 7, '6832A', 1),
(183, 7, '6832B', 1),
(184, 7, '6910Z', 1),
(185, 7, '6920Z', 1),
(186, 7, '7120A', 1),
(187, 7, '7500Z', 1),
(188, 7, '8411Z', 1),
(189, 7, '8412Z', 1),
(190, 7, '8413Z', 1),
(191, 7, '8421Z', 1),
(192, 7, '8422Z', 1),
(193, 7, '8423Z', 1),
(194, 7, '8424Z', 1),
(195, 7, '8425Z', 1),
(196, 7, '8430A', 1),
(197, 7, '8430B', 1),
(198, 7, '8430C', 1),
(199, 7, '8510Z', 1),
(200, 7, '8520Z', 1),
(201, 7, '8531Z', 1),
(202, 7, '8532Z', 1),
(203, 7, '8541Z', 1),
(204, 7, '8542Z', 1),
(205, 7, '8551Z', 1),
(206, 7, '8552Z', 1),
(207, 7, '8553Z', 1),
(208, 7, '8559A', 1),
(209, 7, '8559B', 1),
(210, 7, '8560Z', 1),
(211, 7, '8610Z', 1),
(212, 7, '8621Z', 1),
(213, 7, '8622A', 1),
(214, 7, '8622B', 1),
(215, 7, '8622C', 1),
(216, 7, '8623Z', 1),
(217, 7, '8690A', 1),
(218, 7, '8690B', 1),
(219, 7, '8690C', 1),
(220, 7, '8690D', 1),
(221, 7, '8690E', 1),
(222, 7, '8690F', 1),
(223, 7, '8710A', 1),
(224, 7, '8710B', 1),
(225, 7, '8710C', 1),
(226, 7, '4791B', 1),
(227, 7, '6312Z', 1),
(228, 7, '4773Z', 1),
(229, 7, '4774Z', 1),
(230, 7, '4778A', 1),
(231, 7, '4932Z', 1),
(232, 9, '5819Z', 1),
(233, 9, '6312Z', 1);

-- --------------------------------------------------------

--
-- Structure de la table `regions_vouchers`
--

CREATE TABLE `regions_vouchers` (
  `id` int(11) NOT NULL,
  `regions_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `refund` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `regions_vouchers`
--

INSERT INTO `regions_vouchers` (`id`, `regions_id`, `name`, `min`, `max`, `refund`, `amount`) VALUES
(1, 1, 'Chéque E-commerce', 500, 10000, 50, 5000),
(2, 2, 'Chéque commerce connecter', 300, 3000, 50, 1500),
(3, 3, 'Chéque booster TPE numérique', 3000, 30000, 40, 12000),
(4, 4, 'Aide régional mon commerce en ligne', 600, 3000, 50, 1500),
(5, 4, 'Aide régional mon commerce en ligne', 100, 500, 100, 500),
(6, 5, 'Chéque impulsion transition numérique', 2000, 10000, 50, 5000),
(7, 6, 'Chéque soutien à la digitalisation', 1000, 6000, 100, 6000),
(8, 7, 'Chéque investissement numérique - PDLIN', 5000, 30000, 50, 15000),
(9, 8, 'Chèque Numérique : Renforcement du dispositif dans le cadre de la crise sanitaire du Covid-19', 300, 4000, 80, 3200),
(10, 9, 'Pass numarique : aide à la transformation numérique', 300, 10000, 50, 500),
(11, 10, 'Chéque tic', 300, 12500, 80, 10000);

-- --------------------------------------------------------

--
-- Structure de la table `services_grants`
--

CREATE TABLE `services_grants` (
  `id` int(11) NOT NULL,
  `transitions_id` int(11) NOT NULL,
  `orientation` varchar(255) DEFAULT NULL,
  `turnovers_id` int(11) NOT NULL,
  `grants` int(11) DEFAULT NULL,
  `original_price` int(11) DEFAULT NULL,
  `sell_price` int(11) DEFAULT NULL,
  `budget` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `services_grants`
--

INSERT INTO `services_grants` (`id`, `transitions_id`, `orientation`, `turnovers_id`, `grants`, `original_price`, `sell_price`, `budget`) VALUES
(1, 22, 'site vitrine starter', 1, 5100, 5999, 899, 500),
(2, 22, 'site vitrine sur mesur', 2, 3000, 5999, 2999, 3000),
(3, 22, 'site vitrine sur mesur', 3, 2850, 5999, 3149, 3200),
(4, 22, 'site vitrine sur mesur', 4, 2750, 5999, 3249, 3300),
(5, 22, 'site vitrine sur mesur', 5, 2650, 5999, 3349, 3400),
(6, 22, 'site vitrine sur mesur', 6, 2550, 5999, 3449, 3500),
(7, 22, 'site vitrine sur mesur', 7, 2450, 5999, 3549, 3600),
(8, 22, 'site vitrine sur mesur', 8, 2350, 5999, 3649, 3700),
(9, 22, 'site vitrine sur mesur', 9, 2150, 5999, 3849, 3900),
(10, 22, 'site vitrine sur mesur', 10, 1950, 5999, 4049, 4100),
(11, 22, 'site vitrine sur mesur', 11, 1750, 5999, 4249, 4300),
(12, 22, 'site vitrine sur mesur', 12, 1550, 5999, 4449, 4500),
(13, 22, 'site vitrine sur mesur', 13, 1350, 5999, 4649, 4700),
(14, 22, 'site vitrine sur mesur', 14, 1150, 5999, 4849, 4900),
(15, 22, 'site vitrine sur mesur', 15, 950, 5999, 5049, 5100),
(16, 22, 'site vitrine sur mesur', 16, 750, 5999, 5249, 5300),
(17, 22, 'site vitrine sur mesur', 17, 550, 5999, 5449, 5500),
(18, 22, 'site vitrine sur mesur', 18, 350, 5999, 5649, 5700),
(19, 22, 'site vitrine sur mesur', 19, 350, 5999, 5649, 5700),
(20, 22, 'site vitrine sur mesur', 20, 350, 5999, 5649, 5700),
(21, 22, 'site vitrine sur mesur', 21, 350, 5999, 5649, 5700),
(22, 22, 'site vitrine sur mesur', 22, 350, 5999, 5649, 5700),
(23, 22, 'site vitrine sur mesur', 23, 350, 5999, 5649, 5700),
(24, 22, 'site vitrine sur mesur', 24, 350, 5999, 5649, 5700),
(25, 22, 'site vitrine sur mesur', 25, 350, 5999, 5649, 5700),
(26, 22, 'site vitrine sur mesur', 26, 350, 5999, 5649, 5700),
(27, 22, 'site vitrine sur mesur', 27, 350, 5999, 5649, 5700),
(28, 22, 'site vitrine sur mesur', 28, 350, 5999, 5649, 5700),
(29, 22, 'site vitrine sur mesur', 29, 350, 5999, 5649, 5700),
(30, 22, 'site vitrine sur mesur', 30, 350, 5999, 5649, 5700),
(31, 22, 'site vitrine sur mesur', 31, 350, 5999, 5649, 5700),
(32, 22, 'site vitrine sur mesur', 32, 350, 5999, 5649, 5700),
(33, 22, 'site vitrine sur mesur', 33, 350, 5999, 5649, 5700),
(34, 22, 'site vitrine sur mesur', 34, 350, 5999, 5649, 5700),
(35, 22, 'site vitrine sur mesur', 35, 350, 5999, 5649, 5700),
(36, 22, 'site vitrine sur mesur', 36, 350, 5999, 5649, 5700),
(37, 22, 'site vitrine sur mesur', 37, 350, 5999, 5649, 5700),
(38, 22, 'site vitrine sur mesur', 38, 350, 5999, 5649, 5700),
(39, 20, 'Market place', 1, 10250, 20489, 10239, 10300),
(40, 20, 'Market place', 2, 9750, 20489, 10739, 10800),
(41, 20, 'Market place', 3, 9250, 20489, 11239, 11300),
(42, 20, 'Market place', 4, 8750, 20489, 11739, 11800),
(43, 20, 'Market place', 5, 7800, 20489, 12689, 12700),
(44, 20, 'Market place', 6, 7300, 20489, 13189, 13200),
(45, 20, 'Market place', 7, 6800, 20489, 13689, 13700),
(46, 20, 'Market place', 8, 6300, 20489, 14189, 14200),
(47, 20, 'Market place', 9, 5800, 20489, 14689, 14700),
(48, 20, 'Market place', 10, 5500, 20489, 14989, 15000),
(49, 20, 'Market place', 11, 5000, 20489, 15489, 15500),
(50, 20, 'Market place', 12, 4500, 20489, 15989, 16000),
(51, 20, 'Market place', 13, 4000, 20489, 16489, 16500),
(52, 20, 'Market place', 14, 3500, 20489, 16989, 17000),
(53, 20, 'Market place', 15, 3500, 20489, 16989, 17000),
(54, 20, 'Market place', 16, 3500, 20489, 16989, 17000),
(55, 20, 'Market place', 17, 3500, 20489, 16989, 17000),
(56, 20, 'Market place', 18, 3500, 20489, 16989, 17000),
(57, 20, 'Market place', 19, 3500, 20489, 16989, 17000),
(58, 20, 'Market place', 20, 3500, 20489, 16989, 17000),
(59, 20, 'Market place', 21, 3500, 20489, 16989, 17000),
(60, 20, 'Market place', 22, 3500, 20489, 16989, 17000),
(61, 20, 'Market place', 23, 3500, 20489, 16989, 17000),
(62, 20, 'Market place', 24, 3500, 20489, 16989, 17000),
(63, 20, 'Market place', 25, 3500, 20489, 16989, 17000),
(64, 20, 'Market place', 26, 3500, 20489, 16989, 17000),
(65, 20, 'Market place', 27, 3500, 20489, 16989, 17000),
(66, 20, 'Market place', 28, 3500, 20489, 16989, 17000),
(67, 20, 'Market place', 29, 3500, 20489, 16989, 17000),
(68, 20, 'Market place', 30, 3500, 20489, 16989, 17000),
(69, 20, 'Market place', 31, 3500, 20489, 16989, 17000),
(70, 20, 'Market place', 32, 3500, 20489, 16989, 17000),
(71, 20, 'Market place', 33, 3500, 20489, 16989, 17000),
(72, 20, 'Market place', 34, 3500, 20489, 16989, 17000),
(73, 20, 'Market place', 35, 3500, 20489, 16989, 17000),
(74, 20, 'Market place', 36, 3500, 20489, 16989, 17000),
(75, 20, 'Market place', 37, 3500, 20489, 16989, 17000),
(76, 20, 'Market place', 38, 3500, 20489, 16989, 17000),
(77, 31, 'App starter', 1, 6000, 12000, 6000, 6500),
(78, 31, 'App starter', 2, 6000, 12000, 6000, 7000),
(79, 31, 'App starter', 3, 6000, 12000, 6000, 7500),
(80, 31, 'App sur mesur ', 4, 10000, 22999, 12000, 13000),
(81, 31, 'App sur mesur ', 5, 8851, 22999, 14148, 14200),
(82, 31, 'App sur mesur ', 6, 7702, 22999, 15297, 15300),
(83, 31, 'App sur mesur ', 7, 6553, 22999, 16446, 16500),
(84, 31, 'App sur mesur ', 8, 5404, 22999, 17595, 17600),
(85, 31, 'App sur mesur ', 9, 4255, 22999, 18744, 18700),
(86, 31, 'App sur mesur ', 10, 3106, 22999, 19893, 19900),
(87, 31, 'App sur mesur ', 11, 1957, 22999, 21042, 21100),
(88, 31, 'App sur mesur ', 12, 1957, 22999, 21042, 21100),
(89, 31, 'App sur mesur ', 13, 1957, 22999, 21042, 21100),
(90, 31, 'App sur mesur ', 14, 1957, 22999, 21042, 21100),
(91, 31, 'App sur mesur ', 15, 1957, 22999, 21042, 21100),
(92, 31, 'App sur mesur ', 16, 1957, 22999, 21042, 21100),
(93, 31, 'App sur mesur ', 17, 1957, 22999, 21042, 21100),
(94, 31, 'App sur mesur ', 18, 1957, 22999, 21042, 21100),
(95, 31, 'App sur mesur ', 19, 1957, 22999, 21042, 21100),
(96, 31, 'App sur mesur ', 20, 1957, 22999, 21042, 21100),
(97, 31, 'App sur mesur ', 21, 1957, 22999, 21042, 21100),
(98, 31, 'App sur mesur ', 22, 1957, 22999, 21042, 21100),
(99, 31, 'App sur mesur ', 23, 1957, 22999, 21042, 21100),
(100, 31, 'App sur mesur ', 24, 1957, 22999, 21042, 21100),
(101, 31, 'App sur mesur ', 25, 1957, 22999, 21042, 21100),
(102, 31, 'App sur mesur ', 26, 1957, 22999, 21042, 21100),
(103, 31, 'App sur mesur ', 27, 1957, 22999, 21042, 21100),
(104, 31, 'App sur mesur ', 28, 1957, 22999, 21042, 21100),
(105, 31, 'App sur mesur ', 29, 1957, 22999, 21042, 21100),
(106, 31, 'App sur mesur ', 30, 1957, 22999, 21042, 21100),
(107, 31, 'App sur mesur ', 31, 1957, 22999, 21042, 21100),
(108, 31, 'App sur mesur ', 32, 1957, 22999, 21042, 21100),
(109, 31, 'App sur mesur ', 33, 1957, 22999, 21042, 21100),
(110, 31, 'App sur mesur ', 34, 1957, 22999, 21042, 21100),
(111, 31, 'App sur mesur ', 35, 1957, 22999, 21042, 21100),
(112, 31, 'App sur mesur ', 36, 1957, 22999, 21042, 21100),
(113, 31, 'App sur mesur ', 37, 1957, 22999, 21042, 21100),
(114, 31, 'App sur mesur ', 38, 1957, 22999, 21042, 21100),
(115, 24, 'Site ecommerce starter', 1, 8400, 9599, 1199, 1500),
(116, 24, 'Site ecommerce starter', 2, 8400, 9599, 1199, 2000),
(117, 24, 'Site ecommerce 50-100 produits', 3, 7000, 9599, 2599, 2600),
(118, 24, 'Site ecommerce 50-100 produits', 4, 7000, 9599, 2599, 3000),
(119, 24, 'Site ecommerce 50-100 produits', 5, 7000, 9599, 2599, 3500),
(120, 24, 'Site ecommerce 150-500 produits', 6, 5000, 9599, 4599, 4600),
(121, 24, 'Site ecommerce 150-500 produits', 7, 5000, 9599, 4599, 5000),
(122, 24, 'Site ecommerce 150-500 produits', 8, 5000, 9599, 4599, 5500),
(123, 24, 'Site ecommerce Produit illimité', 9, 3600, 9599, 5999, 7000),
(124, 24, 'Site ecommerce Produit illimité', 10, 3600, 9599, 5999, 7100),
(125, 24, 'Site ecommerce Produit illimité', 11, 3600, 9599, 5999, 7200),
(126, 24, 'Site ecommerce Produit illimité', 12, 3600, 9599, 5999, 7300),
(127, 24, 'Site ecommerce Produit illimité', 13, 3600, 9599, 5999, 7400),
(128, 24, 'Site ecommerce Produit illimité', 14, 3600, 9599, 5999, 7500),
(129, 24, 'Site ecommerce sur mesur illimité', 15, 2880, 9599, 6719, 7600),
(130, 24, 'Site ecommerce sur mesur illimité', 16, 2400, 9599, 7199, 8000),
(131, 24, 'Site ecommerce sur mesur illimité', 17, 1920, 9599, 7679, 8500),
(132, 24, 'Site ecommerce sur mesur illimité', 18, 1440, 9599, 8159, 9000),
(133, 24, 'Site ecommerce sur mesur illimité', 19, 950, 9599, 8650, 10000),
(134, 24, 'Site ecommerce sur mesur illimité', 20, 950, 9599, 8650, 10000),
(135, 24, 'Site ecommerce sur mesur illimité', 21, 950, 9599, 8650, 10000),
(136, 24, 'Site ecommerce sur mesur illimité', 22, 950, 9599, 8650, 10000),
(137, 24, 'Site ecommerce sur mesur illimité', 23, 950, 9599, 8650, 10000),
(138, 24, 'Site ecommerce sur mesur illimité', 24, 950, 9599, 8650, 10000),
(139, 24, 'Site ecommerce sur mesur illimité', 25, 950, 9599, 8650, 10000),
(140, 24, 'Site ecommerce sur mesur illimité', 26, 950, 9599, 8650, 10000),
(141, 24, 'Site ecommerce sur mesur illimité', 27, 950, 9599, 8650, 10000),
(142, 24, 'Site ecommerce sur mesur illimité', 28, 950, 9599, 8650, 10000),
(143, 24, 'Site ecommerce sur mesur illimité', 29, 950, 9599, 8650, 10000),
(144, 24, 'Site ecommerce sur mesur illimité', 30, 950, 9599, 8650, 10000),
(145, 24, 'Site ecommerce sur mesur illimité', 31, 950, 9599, 8650, 10000),
(146, 24, 'Site ecommerce sur mesur illimité', 32, 950, 9599, 8650, 10000),
(147, 24, 'Site ecommerce sur mesur illimité', 33, 950, 9599, 8650, 10000),
(148, 24, 'Site ecommerce sur mesur illimité', 34, 950, 9599, 8650, 10000),
(149, 24, 'Site ecommerce sur mesur illimité', 35, 950, 9599, 8650, 10000),
(150, 24, 'Site ecommerce sur mesur illimité', 36, 950, 9599, 8650, 10000),
(151, 24, 'Site ecommerce sur mesur illimité', 37, 950, 9599, 8650, 10000),
(152, 24, 'Site ecommerce sur mesur illimité', 38, 950, 9599, 8650, 10000),
(153, 25, 'CRM /ERP', 1, 11999, 29999, 18000, 19500),
(154, 25, 'CRM /ERP', 2, 10500, 29999, 19499, 20000),
(155, 25, 'CRM /ERP', 3, 9000, 29999, 20999, 21000),
(156, 25, 'CRM /ERP', 4, 8100, 29999, 21899, 22000),
(157, 25, 'CRM /ERP', 5, 7800, 29999, 22199, 22200),
(158, 25, 'CRM /ERP', 6, 7500, 29999, 22499, 22500),
(159, 25, 'CRM /ERP', 7, 7200, 29999, 22799, 22800),
(160, 25, 'CRM /ERP', 8, 6600, 29999, 23399, 23400),
(161, 25, 'CRM /ERP', 9, 6300, 29999, 23699, 23700),
(162, 25, 'CRM /ERP', 10, 6000, 29999, 23999, 24000),
(163, 25, 'CRM /ERP', 11, 5700, 29999, 24299, 24300),
(164, 25, 'CRM /ERP', 12, 5400, 29999, 24599, 24600),
(165, 25, 'CRM /ERP', 13, 5100, 29999, 24899, 24900),
(166, 25, 'CRM /ERP', 14, 4800, 29999, 25199, 25200),
(167, 25, 'CRM /ERP', 15, 4500, 29999, 25499, 25500),
(168, 25, 'CRM /ERP', 16, 4500, 29999, 25499, 25500),
(169, 25, 'CRM /ERP', 17, 4500, 29999, 25499, 25500),
(170, 25, 'CRM /ERP', 18, 4500, 29999, 25499, 25500),
(171, 25, 'CRM /ERP', 19, 4500, 29999, 25499, 25500),
(172, 25, 'CRM /ERP', 20, 4500, 29999, 25499, 25500),
(173, 25, 'CRM /ERP', 21, 4500, 29999, 25499, 25500),
(174, 25, 'CRM /ERP', 22, 4500, 29999, 25499, 25500),
(175, 25, 'CRM /ERP', 23, 4500, 29999, 25499, 25500),
(176, 25, 'CRM /ERP', 24, 4500, 29999, 25499, 25500),
(177, 25, 'CRM /ERP', 25, 4500, 29999, 25499, 25500),
(178, 25, 'CRM /ERP', 26, 4500, 29999, 25499, 25500),
(179, 25, 'CRM /ERP', 27, 4500, 29999, 25499, 25500),
(180, 25, 'CRM /ERP', 28, 4500, 29999, 25499, 25500),
(181, 25, 'CRM /ERP', 29, 4500, 29999, 25499, 25500),
(182, 25, 'CRM /ERP', 30, 4500, 29999, 25499, 25500),
(183, 25, 'CRM /ERP', 31, 4500, 29999, 25499, 25500),
(184, 25, 'CRM /ERP', 32, 4500, 29999, 25499, 25500),
(185, 25, 'CRM /ERP', 33, 4500, 29999, 25499, 25500),
(186, 25, 'CRM /ERP', 34, 4500, 29999, 25499, 25500),
(187, 25, 'CRM /ERP', 35, 4500, 29999, 25499, 25500),
(188, 25, 'CRM /ERP', 36, 4500, 29999, 25499, 25500),
(189, 25, 'CRM /ERP', 37, 4500, 29999, 25499, 25500),
(190, 25, 'CRM /ERP', 38, 4500, 29999, 25499, 25500),
(191, 43, 'Community manager pack1', 1, 675, 1499, 824, 830),
(192, 43, 'Community manager pack1', 2, 660, 1499, 839, 840),
(193, 43, 'Community manager pack1', 3, 645, 1499, 854, 860),
(194, 43, 'Community manager pack1', 4, 630, 1499, 869, 870),
(195, 43, 'Community manager pack1', 5, 615, 1499, 884, 890),
(196, 43, 'Community manager pack1', 6, 600, 1499, 899, 900),
(197, 43, 'Community manager pack1', 7, 580, 1499, 919, 920),
(198, 43, 'Community manager pack1', 8, 570, 1499, 929, 930),
(199, 43, 'Community manager pack1', 9, 555, 1499, 944, 950),
(200, 43, 'Community manager pack1', 10, 540, 1499, 959, 1000),
(201, 43, 'Community manager pack1', 11, 375, 1499, 1124, 1200),
(202, 43, 'Community manager pack1', 12, 360, 1499, 1139, 1240),
(203, 43, 'Community manager pack1', 13, 345, 1499, 1154, 1260),
(204, 43, 'Community manager pack1', 14, 330, 1499, 1169, 1270),
(205, 43, 'Community manager pack1', 15, 300, 1499, 1199, 1400),
(206, 43, 'Community manager pack1', 16, 300, 1499, 1199, 1400),
(207, 43, 'Community manager pack1', 17, 300, 1499, 1199, 1400),
(208, 43, 'Community manager pack1', 18, 300, 1499, 1199, 1400),
(209, 43, 'Community manager pack1', 19, 300, 1499, 1199, 1400),
(210, 43, 'Community manager pack1', 20, 300, 1499, 1199, 1400),
(211, 43, 'Community manager pack1', 21, 300, 1499, 1199, 1400),
(212, 43, 'Community manager pack1', 22, 300, 1499, 1199, 1400),
(213, 43, 'Community manager pack1', 23, 300, 1499, 1199, 1400),
(214, 43, 'Community manager pack1', 24, 300, 1499, 1199, 1400),
(215, 43, 'Community manager pack1', 25, 300, 1499, 1199, 1400),
(216, 43, 'Community manager pack1', 26, 300, 1499, 1199, 1400),
(217, 43, 'Community manager pack1', 27, 300, 1499, 1199, 1400),
(218, 43, 'Community manager pack1', 28, 300, 1499, 1199, 1400),
(219, 43, 'Community manager pack1', 29, 300, 1499, 1199, 1400),
(220, 43, 'Community manager pack1', 30, 300, 1499, 1199, 1400),
(221, 43, 'Community manager pack1', 31, 300, 1499, 1199, 1400),
(222, 43, 'Community manager pack1', 32, 300, 1499, 1199, 1400),
(223, 43, 'Community manager pack1', 33, 300, 1499, 1199, 1400),
(224, 43, 'Community manager pack1', 34, 300, 1499, 1199, 1400),
(225, 43, 'Community manager pack1', 35, 300, 1499, 1199, 1400),
(226, 43, 'Community manager pack1', 36, 300, 1499, 1199, 1400),
(227, 43, 'Community manager pack1', 37, 300, 1499, 1199, 1400),
(228, 43, 'Community manager pack1', 38, 300, 1499, 1199, 1400),
(229, 44, 'Community manager pack2', 1, 1265, 2529, 1264, 1270),
(230, 44, 'Community manager pack2', 2, 1240, 2529, 1289, 1290),
(231, 44, 'Community manager pack2', 3, 1140, 2529, 1389, 1390),
(232, 44, 'Community manager pack2', 4, 1087, 2529, 1442, 1450),
(233, 44, 'Community manager pack2', 5, 1011, 2529, 1518, 1520),
(234, 44, 'Community manager pack2', 6, 960, 2529, 1569, 1570),
(235, 44, 'Community manager pack2', 7, 935, 2529, 1594, 1600),
(236, 44, 'Community manager pack2', 8, 910, 2529, 1619, 1620),
(237, 44, 'Community manager pack2', 9, 860, 2529, 1669, 1670),
(238, 44, 'Community manager pack2', 10, 835, 2529, 1694, 1700),
(239, 44, 'Community manager pack2', 11, 760, 2529, 1769, 1770),
(240, 44, 'Community manager pack2', 12, 733, 2529, 1796, 1800),
(241, 44, 'Community manager pack2', 13, 709, 2529, 1820, 1900),
(242, 44, 'Community manager pack2', 14, 633, 2529, 1896, 2000),
(243, 44, 'Community manager pack2', 15, 633, 2529, 1896, 2000),
(244, 44, 'Community manager pack2', 16, 633, 2529, 1896, 2000),
(245, 44, 'Community manager pack2', 17, 633, 2529, 1896, 2000),
(246, 44, 'Community manager pack2', 18, 633, 2529, 1896, 2000),
(247, 44, 'Community manager pack2', 19, 633, 2529, 1896, 2000),
(248, 44, 'Community manager pack2', 20, 633, 2529, 1896, 2000),
(249, 44, 'Community manager pack2', 21, 633, 2529, 1896, 2000),
(250, 44, 'Community manager pack2', 22, 633, 2529, 1896, 2000),
(251, 44, 'Community manager pack2', 23, 633, 2529, 1896, 2000),
(252, 44, 'Community manager pack2', 24, 633, 2529, 1896, 2000),
(253, 44, 'Community manager pack2', 25, 633, 2529, 1896, 2000),
(254, 44, 'Community manager pack2', 26, 633, 2529, 1896, 2000),
(255, 44, 'Community manager pack2', 27, 633, 2529, 1896, 2000),
(256, 44, 'Community manager pack2', 28, 633, 2529, 1896, 2000),
(257, 44, 'Community manager pack2', 29, 633, 2529, 1896, 2000),
(258, 44, 'Community manager pack2', 30, 633, 2529, 1896, 2000),
(259, 44, 'Community manager pack2', 31, 633, 2529, 1896, 2000),
(260, 44, 'Community manager pack2', 32, 633, 2529, 1896, 2000),
(261, 44, 'Community manager pack2', 33, 633, 2529, 1896, 2000),
(262, 44, 'Community manager pack2', 34, 633, 2529, 1896, 2000),
(263, 44, 'Community manager pack2', 35, 633, 2529, 1896, 2000),
(264, 44, 'Community manager pack2', 36, 633, 2529, 1896, 2000),
(265, 44, 'Community manager pack2', 37, 633, 2529, 1896, 2000),
(266, 44, 'Community manager pack2', 38, 633, 2529, 1896, 2000);

-- --------------------------------------------------------

--
-- Structure de la table `transitions`
--

CREATE TABLE `transitions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `logo` longtext NOT NULL,
  `desc` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `transitions`
--

INSERT INTO `transitions` (`id`, `name`, `category`, `logo`, `desc`) VALUES
(1, 'Création logo', 'Graphique', 'Creation logo', 'Booster votre image par la création d\'une identité visuelle qui vous démarquera de la concurrence.'),
(2, 'Refonte logo', 'Graphique', 'Refonte logo', 'Remettre votre logo au goût du jour pour concéder à votre entreprise une nouvelle ére.'),
(3, 'Carte de visite', 'Graphique', 'Carte de visite', 'La carte visite est la route premiére vitrine de votre entreprise. Elle doit refléter l\'image que vous portez à votre entreprise.'),
(4, 'Flayer commerciale', 'Graphique', 'Flayer commerciale', 'Conçu pour susciter l\'intérêt des prospects, un flyer contenant un bon design et un message percutant est un gage de réussite.'),
(5, 'Carte de fidélité', 'Graphique', 'Carte de fidélité', 'Attribué des avantages sous formes de services, cadeaux et remises à vos clients à travers des cartes de fidélité personnalisées... quoi de mieux pour les gâter et donc les fidéliser.'),
(6, 'Poster / Affiche', 'Graphique', 'Poster-Affiche', 'Le poster ou l\'affiche vous permet de toucher une large audience. Considérer comme une affiche à portée de main, le poster est idéal pour insérer plus de détails que dans une affiche.'),
(7, 'Dépliant', 'Graphique', 'Dépliant', 'Les dépliants vous permettront de présenter tout votre panel de prestations. Parfait pour faire passer un message captivant.'),
(8, 'Plaquette', 'Graphique', 'Plaquette', 'La plaquette permet de mettre en avant un message, une offre ou un service. Utile pour les commerciaux de l’entreprise qui peuvent prendre appui sur la plaquette commerciale pour prospecter.'),
(9, 'Retouche photo', 'Graphique', 'Retouche photo', 'Améliorer la qualité de la photo, la sublimer ou la retoucher implique un plus grand intérêt au produit ! Indispensable pour cibler un plus large public.'),
(10, 'Charte graphique', 'Graphique', 'Charte graphique', 'La charte graphique vous permet de maintenir une cohérence autour de votre image. C’est la Bible de votre identité visuelle et est indispensable pour toute conception graphique.'),
(11, 'Signature mail', 'Graphique', 'Signature mail', 'Considérée comme la carte visite commerciale, la signature mail est indispensable pour mettre en avant l’atout de votre entreprise ou son contenu.'),
(12, 'Menu restaurant', 'Graphique', 'Menu restaurant', 'Votre menu reflète toute l’image que vous portez à votre établissement. A l’ère du numérique, plus les prospects ont des informations détaillées sur ce que vous proposez, plus ils sont tentés d’essayer.'),
(13, 'Cover single', 'Graphique', 'Cover single', 'Le cover Single est le support visuel de l\'univers de l\'artiste !Il nous est donc indispensable de mettre la lumière sur l’originalité du single à travers sa pochette d’album.'),
(14, 'Pochette à rabat', 'Graphique', 'Pochette à rabat', 'La pochette à rabat est parfaite pour introduire des fichiers produits ou techniques, un devis ou un contrat. Elle est considérée jusqu’à maintenant un document commerciale incontournable.'),
(15, 'Catalogue', 'Graphique', 'Catalogue', 'Idéal pour promouvoir une gamme de produits auprès des consommateurs. En format digital ou en souvenir papier ; le catalogue reste l’outil de communication le plus explicite.'),
(16, 'Couverture de livre', 'Graphique', 'Couverture de livre', 'Une couverture de livre pertinente se doit de constituer une vraie étape dans le marketing de l’éditeur.(trice) ou de l’auteur(e). C’est la première vitrine de ce que le livre contient.'),
(17, 'Kit stand salon', 'Graphique', 'Kit stand salon', 'Pour vos expositions ou forum, le KIT Stand Salon est idéal pour captiver l’intérêt des visiteurs et professionnels.'),
(18, 'Bon de commande', 'Graphique', 'Bon de commande', 'Le bon de commande est un document qui vous permet de définir et valider les modalités de prestation.'),
(19, 'Packaging', 'Graphique', 'Packaging', 'Outre ses fonctions physiques, le packaging est incontestablement la vitrine de toute entreprise. Il permet de communiquer toutes les informations sur le produit au consommateur. C\'est un vecteur de sens, de signification.'),
(20, 'Site marketplace', 'Services', 'Site marketplace', 'Un site web marketplace permet de mettre en relation des vendeurs et des acheteurs, particuliers ou professionnels.'),
(21, 'SEO', 'Développement', 'SEO', 'Le référencement naturel (SEO) consiste souvent à apporter de petites modifications à certaines parties de votre site.'),
(22, 'Site vitrine', 'Services', 'Site vitrine', 'Site vitrine ergonomique est un moyen efficace de parler de vos services.'),
(24, 'Site e-commerce', 'Services', 'Site e-commerce', 'Site web clé en main e-commerce est éligible à la remise de 30% pris en charge par l\'état IDF fond de solidarité pour les créateurs d\'entreprises.'),
(25, 'ERP / CRM', 'Services', 'ERP-CRM', 'Idéal pour l’organisation et la gestion des ressources de votre entreprise et les relations commerciales. L’objectif étant de relier les différentes activités synchroniser leur fonctionnement.'),
(26, 'Application réseau sociaux', 'Développement', 'Application réseau sociaux', 'Application mobile pour réseau social généraliste ou spécialisé.'),
(27, 'Application marketplace', 'Développement', 'Application marketplace', 'Une application marketplace permet de mettre en relation des vendeurs et des acheteurs, particuliers ou professionnels.'),
(28, 'Application jeux mobile', 'Développement', 'Application jeux mobile', 'Une interface intuitive,un langage de script graphique effets particules, et animations.'),
(29, 'Application Marketing', 'Développement', 'Application Marketing', 'Une application mobile marketing ergonomique est un moyen efficace de parlerde vos services.'),
(30, 'Application ERP / CRM', 'Développement', 'Application ERP-CRM', 'Une application ERP / CRM Vous permettra d’automatiser les processus de vente, et de recueillir des informations sur les clients.'),
(31, 'Application E-commerce', 'Services', 'E-commerce', 'Application mobile e-commerce est un moyen efficace pour parler de vos produits et optimiser vos ventes.'),
(32, 'Design applicatif XD', 'Graphique', 'Design applicatif XD', 'Un design applicatif personnalisé et sur mesure, à l’image de votre entreprise.'),
(33, 'Banniére vidéo facebook', 'Montage', 'Banniére vidéo facebook', 'Vitrine de votre facebook c\'est la premiére impression qu\'un utilisateur aura de votre activité. Un facteur décisif.'),
(34, 'Espace et compagne mailing', 'Marketing', 'Espace et compagne mailing', 'Un moyen performant de communiquer sur un événement.'),
(35, 'Banniére réseaux sociaux', 'Graphique', 'Banniére réseaux sociaux', 'La publicité en ligne par excellence pour augmenter vos ventes et développer votre image et notoriété.'),
(36, 'Rédaction web marketing', 'Marketing', 'Rédaction web marketing', 'Rédaction stratégique pour votre audience blog articles avec contenue qualitatif recherche mots-clefs dans votre secteur d\'activité.'),
(37, 'Stratégie marketing', 'Marketing', 'Stratégie marketing', 'Une stratégie marketing bien étudiée et spécifique à votre secteur d\'activité.'),
(38, 'Texte marketing', 'Marketing', 'Texte marketing', 'Un texte marketing captivant, pour votre entreprise.'),
(39, 'Animation logo', 'Montage', 'Animation logo', 'L\'animation de votre logo amplifie l\'impact visuel de votre message et le rendra plus facilement mémorisable.'),
(40, 'Tournage studio', 'Montage', 'Tournage studio', 'Un studio de tournage ( photo / vidéo ) à votre disposition, pour la réalisation d\'un spot publicitaire ou autres.'),
(41, 'Vidéo motion désign', 'Montage', 'Vidéo motion désign', 'Diffuser des informations de façon simple, presenter l’utilisation d’un produit ou le fonctionnement d’un service dans une vidéo motion.'),
(42, 'Vidéo marketing', 'Montage', 'Vidéo marketing', 'La vidéo permet d’expliquerclairement les avantages d’un produit, d’un service, d’un concept ou encore d’une entreprise.'),
(43, 'Community Management (Pack standard)', 'Services', 'Community Management', ''),
(44, 'Community Management (Pack master)', 'Services', 'Community Management', '');

-- --------------------------------------------------------

--
-- Structure de la table `turnovers`
--

CREATE TABLE `turnovers` (
  `id` int(11) NOT NULL,
  `transitions_id` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `turnovers`
--

INSERT INTO `turnovers` (`id`, `transitions_id`, `min`, `max`) VALUES
(1, 22, 5000, 50000),
(2, 22, 50000, 100000),
(3, 22, 100000, 200000),
(4, 22, 200000, 300000),
(5, 22, 300000, 400000),
(6, 22, 400000, 500000),
(7, 25, 500000, 600000),
(8, 25, 600000, 700000),
(9, 25, 700000, 800000),
(10, 25, 800000, 900000),
(11, 25, 900000, 1000000),
(12, 25, 1000000, 1500000),
(13, 20, 1500000, 2000000),
(14, 20, 2000000, 2500000),
(15, 20, 2500000, 3000000),
(16, 20, 3000000, 3500000),
(17, 20, 3500000, 4000000),
(18, 20, 4000000, 4500000),
(19, 31, 4500000, 5000000),
(20, 31, 5000000, 5500000),
(21, 31, 5500000, 6000000),
(22, 31, 6000000, 6500000),
(23, 31, 6500000, 7000000),
(24, 24, 7000000, 7500000),
(25, 24, 7500000, 8000000),
(26, 24, 8000000, 8500000),
(27, 24, 8500000, 9000000),
(28, 24, 9000000, 9500000),
(29, 43, 9500000, 10000000),
(30, 43, 10000000, 10500000),
(31, 43, 10500000, 11000000),
(32, 43, 11000000, 12000000),
(33, 43, 12000000, 13000000),
(34, 44, 13000000, 14000000),
(35, 44, 14000000, 15000000),
(36, 44, 15000000, 20000000),
(37, 44, 20000000, 25000000),
(38, 44, 25000000, 30000000);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `levels_id` int(11) DEFAULT 1,
  `experiences_id` int(11) DEFAULT 1,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `role` enum('tpe','age','col') DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `authorized` tinyint(4) DEFAULT 0,
  `blocked` tinyint(4) DEFAULT 0,
  `stored` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `levels_id`, `experiences_id`, `first_name`, `last_name`, `email`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `authorized`, `blocked`, `stored`) VALUES
(1, 3, 1, 'khalil', 'mecha', 'klilmecha@gmail.com', 'tpe', NULL, '$2y$10$hdMbBWjKcaswZaJJyttp7OqY.2tEU5DbutLgWMFdc5uuFWyypcqs.', NULL, '2021-09-06 09:15:34', '2021-09-11 22:57:56', 0, 0, 0),
(2, 1, 1, 'khawla', NULL, 'khawla1khiari23@gmail.com', 'tpe', '2021-09-14 09:28:34', '$2y$10$1oKO1EiKeCVFcV7JkNjSuOOzgq7aa4.KIV7Lq7G/jZvWJdESPoMMm', NULL, '2021-09-14 11:18:19', '2021-09-15 11:28:39', 0, 0, 0),
(3, 1, 1, 'khawla', NULL, 'khawla1khiari23@gmail.com', 'tpe', NULL, '$2y$10$qZ0WDOADk4j9kRU4YasmFeRPRd1dkhHBg4Vy7IeyOYVgXGh78MMwG', NULL, '2021-09-14 13:56:16', '2021-09-14 13:56:16', 0, 0, 0),
(4, 1, 1, 'khawla', NULL, 'khawla1khiari23@gmail.com', 'tpe', NULL, '$2y$10$YyOA8cInJG9XVqtRN.XqCuUOYJ3mG0vwAQe00dk4m2nUiae.lNIji', NULL, '2021-09-14 13:56:48', '2021-09-14 13:56:48', 0, 0, 0),
(5, 1, 1, 'khawla', NULL, 'khawla1khiari23@gmail.com', 'tpe', NULL, '$2y$10$X7ydzsuJf8WG0o8bQRq7cuDiJwhShJAUqzhhOmMGwCJr5M/9Cndiq', NULL, '2021-09-14 14:04:09', '2021-09-14 14:04:09', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `verifications`
--

CREATE TABLE `verifications` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `used` tinyint(4) NOT NULL DEFAULT 0,
  `deleted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `verifications`
--

INSERT INTO `verifications` (`id`, `users_id`, `type`, `code`, `created_at`, `updated_at`, `used`, `deleted`) VALUES
(1, NULL, 'eligibility', 'oAy4yZdcUaAMQFwmOKN80YYbywtPtLYs', '2021-09-06 09:16:36', '2021-09-06 09:16:36', 0, 0),
(2, NULL, 'eligibility', 'k9nPrCQJGoRbVQ5F0pqa3wRNkEm3wnel', '2021-09-06 09:25:06', '2021-09-06 09:25:06', 0, 0),
(3, NULL, 'eligibility', 'KmnYmTy4WR40NJz6xhFvzTFjuHXN75np', '2021-09-06 09:25:38', '2021-09-06 09:25:38', 0, 0),
(4, NULL, 'eligibility', 'J7eaZiZme1YZqj5jAtPPCZz3ISn9sJ6K', '2021-09-06 09:25:39', '2021-09-06 09:25:39', 0, 0),
(5, NULL, 'eligibility', 'itHKNE4UcyDOxHq9WtR97YuoppbknnvW', '2021-09-06 18:05:07', '2021-09-06 18:05:07', 0, 0),
(6, NULL, 'eligibility', 'kCyIZ827OFgV2DkVbAiJhHwavkC77mJU', '2021-09-06 18:05:41', '2021-09-06 18:05:41', 0, 0),
(7, NULL, 'eligibility', 'Ac5nlpOtbliX4bJtNe3jEf25S0cJFwaM', '2021-09-06 18:06:26', '2021-09-06 18:06:26', 0, 0),
(8, NULL, 'eligibility', 'sMaetUutFkMNBHFPbrokjLPXgeIjSVeM', '2021-09-06 18:07:02', '2021-09-06 18:07:02', 0, 0),
(9, NULL, 'eligibility', 'rYdP3Qkfbay1YkpRhIyZDdI46DXjvCJm', '2021-09-06 18:07:35', '2021-09-06 18:07:35', 0, 0),
(10, NULL, 'eligibility', '2siMYLFxNVjGxiqMtyeRXekKW4qCnyBr', '2021-09-06 18:07:55', '2021-09-06 18:07:55', 0, 0),
(11, NULL, 'eligibility', 'PT2ONLZ9afLBcEjFQMXF31PBPAPH16iI', '2021-09-06 18:09:24', '2021-09-06 18:09:24', 0, 0),
(12, NULL, 'eligibility', 'thC54sbUlopBpggbGSoY9dpaNjSybTom', '2021-09-06 18:09:42', '2021-09-06 18:09:42', 0, 0),
(13, NULL, 'eligibility', 'YE4F1ogNZ4YnveRqaZlJJUz5sQC9FRru', '2021-09-06 18:10:11', '2021-09-06 18:10:11', 0, 0),
(14, NULL, 'eligibility', 'SDQRQuUHL559GpyGNrEo4WmHCg44lH2W', '2021-09-06 18:11:16', '2021-09-06 18:11:16', 0, 0),
(15, NULL, 'eligibility', 'PT5Uf07MFH9MSorQJOiY3gSIINdb4dqT', '2021-09-06 18:11:37', '2021-09-06 18:11:37', 0, 0),
(16, NULL, 'eligibility', 'Cel0DHlTntGRpGLesXmuNoL1kSXFR5y8', '2021-09-06 18:12:12', '2021-09-06 18:12:12', 0, 0),
(17, NULL, 'eligibility', '9wQ3R2IdLPv9K2JiJqZEvB92hJm8mw9l', '2021-09-06 18:12:42', '2021-09-06 18:12:42', 0, 0),
(18, NULL, 'eligibility', 'KZXgZ0IKg88lFQJZYF4Yr0q6IpWVfVly', '2021-09-06 18:13:31', '2021-09-06 18:13:31', 0, 0),
(19, NULL, 'eligibility', 'X0JEe5XcLQy56ojPinOqu0WoAW4q8fUS', '2021-09-06 18:14:36', '2021-09-06 18:14:36', 0, 0),
(20, NULL, 'eligibility', 'fLqVdHNbvmJdZsZskf5hoywd9RCUn618', '2021-09-06 18:16:16', '2021-09-06 18:16:16', 0, 0),
(21, NULL, 'eligibility', 'dtR1B2sbn36mqg9550iLtaXPy7XWzc09', '2021-09-06 18:17:03', '2021-09-06 18:17:03', 0, 0),
(22, NULL, 'eligibility', 'LX3CbQwSWqxf33f0MZQU7qasxixrSL8x', '2021-09-06 18:17:21', '2021-09-06 18:17:21', 0, 0),
(23, NULL, 'eligibility', 'l12g21mhz8kud3PFC42gEerqEHLGRvFq', '2021-09-07 09:55:52', '2021-09-07 09:57:17', 1, 0),
(24, NULL, 'eligibility', 'WHGOJnGPFf5aMX7o0VVc9wSb5D7yoobj', '2021-09-07 10:05:01', '2021-09-07 10:05:01', 0, 0),
(25, NULL, 'eligibility', 'Kv1EX5fPXLIGSrNYh5lb1YnnPTicl3ZX', '2021-09-07 10:05:31', '2021-09-07 10:05:31', 0, 0),
(26, NULL, 'eligibility', 'N3h6yO6P9xF9bvc1A9u9GPnw97Rkj5qK', '2021-09-07 10:08:05', '2021-09-07 10:08:05', 0, 0),
(27, NULL, 'eligibility', 'wCG27EzsC42hL0fpzYU54nwhUb7qkBZx', '2021-09-07 10:09:16', '2021-09-07 10:09:16', 0, 0),
(28, NULL, 'eligibility', 'RH2jPQZ5ENJevcCOX5S26hBgcDnsCXlX', '2021-09-07 10:51:59', '2021-09-07 10:51:59', 0, 0),
(29, NULL, 'eligibility', 'RigZY4eRpSGy3VIaVFZR7QAIbuIeKZLn', '2021-09-07 10:53:15', '2021-09-07 10:53:15', 0, 0),
(30, NULL, 'eligibility', 'K8kMPlodcP3ptK3uhYuLQNnBj98PEx0z', '2021-09-07 10:55:26', '2021-09-07 10:55:26', 0, 0),
(31, NULL, 'eligibility', 'pNSJjsMB4lFfREOxs9XlpwYCLLYNTUdp', '2021-09-07 10:57:26', '2021-09-07 10:57:26', 0, 0),
(32, NULL, 'eligibility', 'iKNxOiyW2UM9UsqpBlBUtDhGFjrRJklQ', '2021-09-07 10:57:28', '2021-09-07 10:57:28', 0, 0),
(33, NULL, 'eligibility', 'a44HaBzDCtayPuoM7Iff7hAHXkM4NXuk', '2021-09-07 10:57:38', '2021-09-07 10:57:38', 0, 0),
(34, NULL, 'eligibility', '1L4WvmOXiS1XrU6ExwRKEJBPuU6dMWmx', '2021-09-07 10:57:41', '2021-09-07 10:57:41', 0, 0),
(35, NULL, 'eligibility', '8s3WKrnoZ7mDk7jTtTOA0V0fLP48Yt10', '2021-09-07 10:58:17', '2021-09-07 10:58:17', 0, 0),
(36, NULL, 'eligibility', '724xL9J1fApEobeksqSM6AhQu7wEJwNe', '2021-09-07 13:59:58', '2021-09-07 13:59:58', 0, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `activities_categories`
--
ALTER TABLE `activities_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_activities_categories_activities1_idx` (`activities_id`);

--
-- Index pour la table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_addresses_users1_idx` (`users_id`),
  ADD KEY `fk_addresses_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_addresses_companies1_idx` (`companies_id`);

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `calendar_events`
--
ALTER TABLE `calendar_events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_calendar_events_users1_idx` (`users_id`),
  ADD KEY `fk_calendar_events_contacts1_idx` (`contacts_id`);

--
-- Index pour la table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_companies_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_companies_activities1_idx` (`activities_id`),
  ADD KEY `fk_companies_turnovers1_idx` (`turnovers_id`);

--
-- Index pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_contacts_users1_idx` (`users_id`);

--
-- Index pour la table `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_counters_users1_idx` (`users_id`);

--
-- Index pour la table `development`
--
ALTER TABLE `development`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_development_contacts1_idx` (`contacts_id`);

--
-- Index pour la table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_eligibility_services_grants1_idx` (`cpn_id`),
  ADD KEY `fk_eligibility_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_eligibility_regions_vouchers1_idx` (`regional_id`);

--
-- Index pour la table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `investment`
--
ALTER TABLE `investment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_investment_contacts1_idx` (`contacts_id`),
  ADD KEY `fk_investment_transitions1_idx` (`service_id`);

--
-- Index pour la table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_meetings_contacts1_idx` (`contacts_id`);

--
-- Index pour la table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Index pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pictures_users1_idx` (`users_id`),
  ADD KEY `fk_pictures_articles1_idx` (`articles_id`);

--
-- Index pour la table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_privileges_pages1_idx` (`pages_id`),
  ADD KEY `fk_privileges_levels1_idx` (`levels_id`);

--
-- Index pour la table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `regions_nafs`
--
ALTER TABLE `regions_nafs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_naf_codes_regions1_idx` (`regions_id`);

--
-- Index pour la table `regions_vouchers`
--
ALTER TABLE `regions_vouchers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vouchers_regions1_idx` (`regions_id`);

--
-- Index pour la table `services_grants`
--
ALTER TABLE `services_grants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_services_grants_transitions1_idx` (`transitions_id`),
  ADD KEY `fk_services_grants_turnovers1_idx` (`turnovers_id`);

--
-- Index pour la table `transitions`
--
ALTER TABLE `transitions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `turnovers`
--
ALTER TABLE `turnovers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_turnovers_transitions1_idx` (`transitions_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_levels1_idx` (`levels_id`),
  ADD KEY `fk_users_experiences1_idx` (`experiences_id`);

--
-- Index pour la table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_verifications_users1_idx` (`users_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `activities_categories`
--
ALTER TABLE `activities_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `calendar_events`
--
ALTER TABLE `calendar_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `counters`
--
ALTER TABLE `counters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `development`
--
ALTER TABLE `development`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `investment`
--
ALTER TABLE `investment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `regions_nafs`
--
ALTER TABLE `regions_nafs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT pour la table `regions_vouchers`
--
ALTER TABLE `regions_vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `services_grants`
--
ALTER TABLE `services_grants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT pour la table `turnovers`
--
ALTER TABLE `turnovers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `activities_categories`
--
ALTER TABLE `activities_categories`
  ADD CONSTRAINT `fk_activities_categories_activities1` FOREIGN KEY (`activities_id`) REFERENCES `activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `fk_addresses_companies1` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_addresses_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_addresses_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `calendar_events`
--
ALTER TABLE `calendar_events`
  ADD CONSTRAINT `fk_calendar_events_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_calendar_events_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `fk_companies_activities1` FOREIGN KEY (`activities_id`) REFERENCES `activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_companies_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_companies_turnovers1` FOREIGN KEY (`turnovers_id`) REFERENCES `turnovers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `fk_contacts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `counters`
--
ALTER TABLE `counters`
  ADD CONSTRAINT `fk_counters_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `development`
--
ALTER TABLE `development`
  ADD CONSTRAINT `fk_development_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `eligibility`
--
ALTER TABLE `eligibility`
  ADD CONSTRAINT `fk_eligibility_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eligibility_regions_vouchers1` FOREIGN KEY (`regional_id`) REFERENCES `regions_vouchers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eligibility_services_grants1` FOREIGN KEY (`cpn_id`) REFERENCES `services_grants` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `investment`
--
ALTER TABLE `investment`
  ADD CONSTRAINT `fk_investment_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_investment_transitions1` FOREIGN KEY (`service_id`) REFERENCES `transitions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `meetings`
--
ALTER TABLE `meetings`
  ADD CONSTRAINT `fk_meetings_contacts1` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `fk_pictures_articles1` FOREIGN KEY (`articles_id`) REFERENCES `articles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pictures_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `privileges`
--
ALTER TABLE `privileges`
  ADD CONSTRAINT `fk_privileges_levels1` FOREIGN KEY (`levels_id`) REFERENCES `levels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_privileges_pages1` FOREIGN KEY (`pages_id`) REFERENCES `pages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `regions_nafs`
--
ALTER TABLE `regions_nafs`
  ADD CONSTRAINT `fk_naf_codes_regions1` FOREIGN KEY (`regions_id`) REFERENCES `regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `regions_vouchers`
--
ALTER TABLE `regions_vouchers`
  ADD CONSTRAINT `fk_vouchers_regions1` FOREIGN KEY (`regions_id`) REFERENCES `regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `services_grants`
--
ALTER TABLE `services_grants`
  ADD CONSTRAINT `fk_services_grants_transitions1` FOREIGN KEY (`transitions_id`) REFERENCES `transitions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_services_grants_turnovers1` FOREIGN KEY (`turnovers_id`) REFERENCES `turnovers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `turnovers`
--
ALTER TABLE `turnovers`
  ADD CONSTRAINT `fk_turnovers_transitions1` FOREIGN KEY (`transitions_id`) REFERENCES `transitions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_experiences1` FOREIGN KEY (`experiences_id`) REFERENCES `experiences` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_levels1` FOREIGN KEY (`levels_id`) REFERENCES `levels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `verifications`
--
ALTER TABLE `verifications`
  ADD CONSTRAINT `fk_verifications_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
