@extends('web.layouts.app')

@section('content')
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800|Poppins&display=swap');

    * {
        margin: 0;
        color: "#111D5E";
        padding: 0;
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
    }

    .cards {
       
        max-width: 1100px;
        margin: 0 auto;
        text-align: center;
        padding: 30px;
    }

    .cards h2.header {
       
        font-size: 40px;
        margin: 0 0 30px 0;
        text-transform: uppercase;
        letter-spacing: 1px;
    }

    .services {
        
        display: flex;
        align-items: center;
    }

    .content {
        
        display: flex;
        flex-wrap: wrap;
        flex: 1;
        margin: 20px;
        padding: 20px;
        border: 2px solid #CE1212;
        border-radius: 4px;
        transition: all .3s ease;
    }

    .content .fab {
        border-radius: 30px;
        font-size: 70px;
        margin: 16px 0;
    }

    .content>* {
        flex: 1 1 100%;
    }

    .content:hover {
        color: white;
    }

    .content-1:hover {
        border-color: #111D5E;
        background: #111D5E;
    }

    .content-1:hover a {
        color: #111D5E;
    }




    .content h2 {
        font-size: 30px;
        margin: 16px 0;
        letter-spacing: 1px;
        text-transform: uppercase;
    }

    .content p {
        font-size: 17px;
        font-family: 'Poppins', sans-serif;
    }

    .content a:hover {
        border-radius: 4px;
    }

    @media (max-width: 900px) {
        .services {
            display: flex;
            flex-direction: column;
        }
    }
</style>
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner" style="height: 401px;">
        <div class="carousel-item active">
            <img src="https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="https://bubblemeeting.net/blog/wp-content/uploads/2020/07/people-meeting-brainstorming-blueprint-design-P47QQG6.jpg" class="d-block w-100" alt="...">
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<p class="font-weight-normal" style="    text-align: center;
    padding-top: 24px;
    color: #CE1212;
    font-size: 30px;">C’est quoi la subvention ?</p>
<p class="font-weight-normal" style="    text-align: center;
    margin-left: 234px;
    margin-right: 234px;
    color: #b5b6b4;">
    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim
    veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea
    commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit
    esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et
    accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis
    dolore te feugait nulla facilisi.
</p>

<div class="cards">

    <div class="services">
        <div class="content content-1">
            <div class="ihref_logo"><img width="40%" src="{{asset('/img/Entreprise.png')}}" alt=" TPE-PME"></div>
            <h2>
                TPE-PME
            </h2>
            <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero earum, placeat eum voluptates quo sunt magnam molestiae quidem, quos amet totam neque voluptas iure aspernatur similique asperiores harum officia ullam? </p>
        </div>
        <div class="content content-1">
            <div class="ihref_logo"><img width="40%" src="{{asset('/img/Agence.png')}}" alt=" Agence"></div>
            <h2>
                Agence
            </h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique, ipsum. Aut dolor enim, suscipit rem quam aliquam fugit dolores, sapiente nemo modi iusto possimus neque quasi vitae, laborum corrupti necessitatibus. </p>

        </div>
        <div class="content content-1">
            <div class="ihref_logo"> <img width="40%" src="{{asset('/img/Collectivité.png')}}" alt="Collectivité"></div>
            <h2>
                Collectivité
            </h2>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis laborum sint natus, sapiente animi officiis nobis velit inventore, quas dicta voluptates commodi quidem provident reprehenderit, suscipit voluptas cum ratione quam. </p>

        </div>

    </div>
</div>

@endsection