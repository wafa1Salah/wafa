<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "id",
        "users_id",
        "state_help",
        "have_website",
        "website_type",
        "website_value",
        "website_link",
        "website_dev_date",
        "have_crm",
        "crm_type",
        "crm_dev",
        "crm_dev_date",
        "agency_name",
        "digital_transitions",
        "budget_investement",
        "first_name",
        "last_name",
        "email",
        "phone",
        "position",
    ];
}
