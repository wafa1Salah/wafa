@extends('web.layouts.app')

@section('content')

<style>
    body {
        color: #1e90ff;
    }


    svg {
        position: fixed;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 300px;
    }

    .path {
        stroke-dashoffset: 100;
        stroke-dasharray: 100;

    }

    .frame {
        height: 500px;
    }
</style>
<div class="frame">

    <svg version="1.2" id="Ebene_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="474.7 269.3 85 32" enable-background="new 474.7 269.3 85 32" xml:space="preserve">

        <path class="path" fill="none" stroke="#1e90ff" stroke-width="0.5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M480.4,285.8c11.9,0,14.1-11,22.1-11s2,20,11.4,20s21.5-11.3,41.2-11.3" />

    </svg>
</div>

<script>
    var path = document.querySelector(".path");
    // Get the actual length of your path.
    var len = path.getTotalLength();
    // Dashes have the exact length of the path.
    path.style.strokeDasharray = len + " " + len;
    // Shift of the length of the path, so the line is quite not visible.
    path.style.strokeDashoffset = len;
    // Attach to the window's scroll event.
    window.addEventListener('scroll', function() {
        // Getting the page dimensions.
        var rect = document.querySelector('html').getBoundingClientRect();
        // Height is the size of the page which is out of screen.
        var height = rect.height - window.innerHeight;
        // Percent of scroll bar. 0 Means the top, 1 the bottom.
        var percent = height < 0 ? 1 :
            -rect.top / height;
        // If you omit the `2 *` you will get a growing only path.
        path.style.strokeDashoffset = len * (1 - 2 * percent);
    }, false);
</script>
@endsection