<?php

namespace App\Http\Controllers;

use App\Mail\MailComment;
use App\Mail\SignupEmail;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
  public function index()
  {
    return view("web.home");
  }
  public function home()
  {
    if (auth()->guest()) {
      return view("web.home");
    } else {
      $articles = DB::table("articles")->join("pictures", "articles.id", "articles_id")->limit(3)->get();

      return view("web.home", ["articles" => $articles]);
    }
  }



  public function not_send(Request $request)
  {
    $commentt = $request->comment;
    if (auth()->guest()) {
      return view('web.connexion.login');
    } else {
      $comment = new Comment();
      $comment->comment = $commentt;
      $comment->user_id = auth()->user()->id;
      $comment->save();
      $request->session()->put('comment', $commentt);
      Mail::send(new MailComment());
      return redirect()->back()->with(['success' => 'Nous avons recu votre avis!']);
    }
  }
}
