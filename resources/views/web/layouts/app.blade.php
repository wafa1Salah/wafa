<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
  <meta name="viewport" content="width=device-width">
  <link type="image/png" rel="icon" href="{{ asset('/img/logo.png') }}" sizes="16x16">
  <title>CPN</title>
  <!-- javascripts -->

  <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src='https://cdn.rawgit.com/nizarmah/calendar-javascript-lib/master/calendarorganizer.min.js'></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  {{-- script pour compteur--}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
  {{-- script pour compteur--}}
  <!-- Styles -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.9.0/main.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel='stylesheet' href='https://cdn.rawgit.com/nizarmah/calendar-javascript-lib/master/calendarorganizer.min.css'>
  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/line-awesome/css/line-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/font-awesome-line-awesome/css/all.min.css">
</head>

<body>
  <header class="primary_header">
    <div class="header_wrapper">
      <!-- <nav class="main_navigation navbar navbar-expand-lg navbar-light" style=" background: Gray;"> -->
      <nav class="main_navigation navbar navbar-expand-lg navbar-light" >
        <div class="navigation_wrapper container-fluid">
          <a class="nav_brand navbar-brand" style="padding-left: 44px;" href="/home">
            @if(auth()->guest())
            <img class="brand_logo d-inline-block align-text-top" src="{{asset('/img/cpn-logo-250.png')}}" alt="" style="width: 100px;
               margin-bottom: 10px;">
            @else
            <img class="brand_logo d-inline-block align-text-top" src="{{asset('/img/logo-cpn2.png')}}" alt="" style="width: 70px;
               margin-bottom: 5px;">
            @endif
          </a>

          <button class="nav_button navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="button_toggle navbar-toggler-icon"></span>
          </button>

          <div class="nav_menu collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="nav_menu_wrapper navbar-nav ms-auto mb-2 mb-lg-0">
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ ( request()->is('home') ) ? 'active' : '' }}" active-color="red" aria-current="page" href="/home">Accueil</a>
              </li>
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ (request()->is('actuality')) ? 'active' : '' }}" active-color="red" href="/actuality">Actualité +</a>
              </li>
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ (request()->route('subevention')) ? 'active' : '' }}" active-color="red" href="{{route('subevention')}}">Subvention</a>
              </li>
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ (request()->is('agenda')) ? 'active' : '' }}" active-color="red" href="{{route('agenda')}}">Agenda +</a>
              </li>
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ (request()->is('about')) ? 'active' : '' }}" active-color="red" href="#about">A propos</a>
              </li>
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ (request()->is('contact')) ? 'active' : '' }}" active-color="red" href="/contact">Contactez-nous</a>
              </li>
              @if(auth()->guest())
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link {{ (request()->is('registre')) ? 'active' : '' }}" active-color="red" href="{{route('registre')}}">Inscription</a>
              </li>
              <li class="menu_item nav-item">
                <a class="menu_item_href nav-link btn btn-danger" active-color="red" style="border: none;
                  background: red;
                  border-radius: 25px;
                  color: white;
                  padding: 5px 15px;" href="{{route('login')}}">Connexion</a>
              </li>
              @else
              <li class="menu_item nav-item">
                <style>
                  .action {}

                  .action .profile img {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    object-fit: cover;

                  }

                  .action .menu::before {
                    content: '';
                    position: absolute;
                    top: -5px;
                    right: 28px;
                    width: 20px;
                    height: 20px;
                    background: #ffffff;
                    transform: rotate(45deg);
                  }

                  .action .menu {
                    position: absolute;
                    top: 120px;
                    right: -10px;
                    padding: 10px 20px;
                    background: #ffffff;
                    width: 200px;
                    box-sizing: 0 5px 25px rgba(0, 0, 0, 0.1);
                    border-radius: 15px;
                    transition: 0.5s;
                    visibility: hidden;
                    opacity: 0;
                  }

                  .action .menu.active {
                    visibility: visible;
                    opacity: 1;
                    margin-top: -40px;
                  }


                  h3 {
                    text-align: center;
                    color: #000000;
                  }

                  .action .menu h3 {
                    width: 100%;
                    font-size: 18px;
                    padding: 20px 0;
                    font-weight: 500;
                    color: #555555;
                    line-height: 1.2rem;
                  }

                  .action .menu h3 span {
                    font-size: 14px;
                    color: #cecece;
                    font-weight: 400;
                  }

                  .action .menu ul li {
                    list-style: none;
                    padding: 10px 0;
                    border-top: 1px solid rgba(0, 0, 0, 0.05);
                    display: flex;
                    align-items: center;
                  }

                  .action .menu ul li img {
                    max-width: 20px;
                    margin-right: 10px;
                    opacity: 0.5;
                    transition: 0.5s;
                  }

                  .action .menu ul li:hover img {
                    opacity: 1;
                  }

                  .action .menu ul li a {
                    display: inline-block;
                    text-decoration: none;
                    color: #000000;
                    font-weight: 500;
                    transition: 0.5s;
                  }

                  .action .menu ul li:hover a {
                    color: #000000;
                  }
                  .primary_header .header_wrapper{
        background:  Gray;
    }
                </style>
                <div class="action">
                  <div class="profile" onclick="menuToogle();">
                    <a class="menu_item_href nav-link btn btn-danger" active-color="red" style="border: none;
                  background: red;
                  border-radius: 25px;
                  color: white;
                  padding: 5px 15px;" href="{{route('profile')}}">{{\Illuminate\Support\Facades\Auth::user()->first_name}}</a>
                  </div>
                  <div class="menu">
                    <h3>Khalil Mecha<br><small>FullStack</small></h3>
                    <ul>
                      <li><img src="https://image.flaticon.com/icons/png/128/1946/1946429.png"><a href="#">Modifier profile</a>
                      </li>
                      <li><img src="https://image.flaticon.com/icons/png/128/3342/3342137.png"><a href="#">Changer votre image</a>
                      </li>
                      <li><img src="https://image.flaticon.com/icons/png/128/3064/3064197.png"><a href="#">Changer mot de
                          passe</a></li>
                      <li><img src="https://image.flaticon.com/icons/png/128/1828/1828427.png">
                        <a href="" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">Déconnexion
                          <form id="logout-form" action="#" method="POST" class="d-none">
                            @csrf
                          </form>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>


                <script>
                  function menuToogle() {
                    const toggleMenu = document.querySelector('.menu');
                    toggleMenu.classList.toggle('active')
                  }
                </script>

              </li>
              @endif
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>

  <main class="primary_body mb-5" id="main">
    @yield('content')
  </main>

  <footer style="background: #111D5E;" class="primary_footer">
    <div class="container p-5">
      <div class="row ">
        <div class="col-md-3 col-xs-12  ">
          <img class="mb-3 brand_logo d-inline-block align-text-top" src="{{asset('/img/logo-cpn3.png')}}" alt="" width="" height="45">
          <p style="color:white;font-size: 13px;">Le Cabinet de Propulsion Numérique aide les entreprises à se propulser numériquement et à bénéficier de financement.
            CPN est un organisme de financement à but non lucratif</p>
          <ul style="margin:0; padding:0; display: flex; flex-direction:row">
            <li style="list-style: none;"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/facebook.gif')}}" alt=""></a></li>
            <li style="list-style: none;margin:0 5px"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/instagram.png')}}" alt=""></a></li>
            <li style="list-style: none;margin:0 5px 0 0"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/twitter.png')}}" alt=""></a></li>
            <li style="list-style: none;"><a href="#"><img style="width:40px; height:40px" src="{{asset('/img/youtube.png')}}" alt=""></a></li>
          </ul>
        </div>

            <div class="col-md-3 col-xs-12 d-flex flex-column align-items-center justify-content-center" style="">
              <h3 style="color: white; font-size: 20px; font-weight: 600;">Réseau sociaux</h3> <br>
              <ul style="width:100%; margin:0;padding:0;margin-left: 139px;font-size: 15px;">
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Instagram</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Youtube</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Linkedin</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Facebook</a></li>
              </ul>
            </div>
            <div class="col-md-3 col-xs-12  d-flex flex-column align-items-center justify-content-center">
              <h3 style="color: white; font-size: 20px; font-weight: 600;">Support</h3> <br>
              <ul style="width:100%; margin:0;padding:0;margin-left: 139px;font-size: 15px;">
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">FAQ</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Inscription</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Actualité</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Contact</a></li>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">Je suis conseillé</a></li>
              </ul>
            </div>
            <div class="col-md-3 col-xs-12  d-flex flex-column align-items-center justify-content-center" style="    margin-top: -47px;">
              <h3 style="color: white; font-size: 20px; font-weight: 600;">Contact</h3> <br>
              <ul style="width:100%; margin:0;padding:0;margin-left: 139px;font-size: 15px; ">
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">+33 6 73 46 65 64</a></li> <br>
                <li style="list-style: none;"><a style="text-decoration: none; color:white" href="#">votreconseiller@cpn-aide-aux-entreprise.com</a></li>
                 <i class="bi bi-messenger"></i>
                <svg xmlns="http://www.w3.org/2000/svg" style="color:#0d6efd"; width="16" height="16" fill="currentColor" class="bi bi-messenger" viewBox="0 0 16 16">
  <path d="M0 7.76C0 3.301 3.493 0 8 0s8 3.301 8 7.76-3.493 7.76-8 7.76c-.81 0-1.586-.107-2.316-.307a.639.639 0 0 0-.427.03l-1.588.702a.64.64 0 0 1-.898-.566l-.044-1.423a.639.639 0 0 0-.215-.456C.956 12.108 0 10.092 0 7.76zm5.546-1.459-2.35 3.728c-.225.358.214.761.551.506l2.525-1.916a.48.48 0 0 1 .578-.002l1.869 1.402a1.2 1.2 0 0 0 1.735-.32l2.35-3.728c.226-.358-.214-.761-.551-.506L9.728 7.381a.48.48 0 0 1-.578.002L7.281 5.98a1.2 1.2 0 0 0-1.735.32z"/>
</svg>
              </ul>

        </div>
      </div>
    </div>
  </footer>
</body>

</html>
