import instances from "./axiosInstance";

export default {
    signin(formData){
        return instances.api().post("signin",formData)
    },
    signup(formData){
        return instances.api().post("signup", formData)
    },
    signout(formData){
        return instances.api().post("signout", formData)
    },
    forget(formData){
        return instances.api().post("forget", formData)
    },

    saveTest(formData){
        return instances.api().post("savetest", formData)
    },
    meetingLinkGenerate(formData){
        return instances.api().post("zoomlink", formData)
    },
    addAxonautData(formData){
        return instances.api().post("axonaut/company/add", formData)
    },
    saveTimer(formData){
        return instances.api().post("savetimer",formData);
    },
    
    getClientsData(){
        return instances.api().get("clients/get");
    },

    siretSearch(siret){
        return instances.siret().get("https://api.societe.com/pro/dev/societe/"+siret+"?token=59a836cb792a3e983c5b3ae5277c0b5b")
    },

    getEvents(){
        return instances.api().get("events/get");
    },
    addEvent(eventData){
        return instances.api().post("events/add",eventData);
    },
    deletEvent(eventId){
        return instances.api().delete("events/"+eventId);
    }
}